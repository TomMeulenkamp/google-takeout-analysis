<?php

require "DatabaseObject.php";
require "Database.php";

class Participant implements DatabaseObject
{
    public static string $HASH_ALGORITHM = "sha256";
    public static int $UID_HASH_LENGTH = 15;
    public static int $SALT_LENGTH = 30;

    private int $uid;
    private string $hashedUid;
    private int $progress;

    /** ONLY STORE THIS DURING INITIALIZATION, NEVER GIVE THIS OUT ON OTHER PLACES! DO NOT STORE IN DB @var string  */
    private ?string $plaintext_password;

    public static array $PROGRESS_OPTIONS = [
        "PRE_ANALYSIS" => 0,
        "ANALYSIS" => 1,
        "POST_ANALYSIS" => 2,
        "FINISHED" => 3,
        0 => "PRE_ANALYSIS",
        1 => "ANALYSIS",
        2 => "POST_ANALYSIS",
        3 => "FINISHED"
    ];

    /**
     * Creates a new participant.
     * @throws Exception If the salt of a user cannot be generated.
     */
    public function __construct(int $uid = -1, string $hashedUid = null, int $progress = 0, string $password = null) {
        if ($uid == -1) {
            // Generates a new uid and participant.
            $db = Database::getInstance();
            $temp = substr(hash("sha256", random_bytes(Participant::$SALT_LENGTH)), 0, 15);
            $password = Participant::generatePassword();
            $password_hash = password_hash($password, PASSWORD_DEFAULT);
            $query = "INSERT INTO Participant (hash, progress, password) VALUES ('{$temp}', 0, '{$password_hash}')";
            $db->performUpdate($query);
            $query = "SELECT uid FROM Participant WHERE hash = '{$temp}'";
            $uid = $db->performQuery($query)[0]['uid'];
            $this->__construct($uid, null, 0, $password);
        } else {
            $this->uid = $uid;
            $this->progress = $progress;
            if($hashedUid == null) {
                $this->hashedUid = $this->generateHash();
            } else {
                $this->hashedUid = $hashedUid;
            }
            $this->plaintext_password = $password;
        }

    }

    public function getUID():int {
        return $this->uid;
    }

    public function getUIDHash(): string {
        return $this->hashedUid;
    }

    /**
     * @return int
     */
    public function getProgress(): int
    {
        return $this->progress;
    }

    /**
     * @param string $progress
     */
    public function setProgress(string $progress): void
    {
        $this->progress = Participant::$PROGRESS_OPTIONS[$progress];
    }

    private function generateHash(): string {
        $hash = hash(Participant::$HASH_ALGORITHM, random_bytes(Participant::$SALT_LENGTH) . $this->uid);
        return substr($hash, 0, Participant::$UID_HASH_LENGTH);
    }

    /**
     * Generates a secure password consisting out of 20 characters.
     * Uses random_int(), since this is cryptically safe.
     * @return string The password
     * @throws Exception If the random_int() function cannot obtain a proper randomness source.
     */
    public static function generatePassword(): string {
        $dictionary = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890!@#$%^&*()-_+=?";
        $password = "";
        for($i = 0; $i < 20; $i++) {
            $index = random_int(0, strlen($dictionary) - 1);
            $password .= substr($dictionary, $index, 1);
        }
        return $password;
    }

    public function getAnswers(): array {
        $query = "SELECT Question.qid, question, value ".
            "FROM Question, Participant, Answer, AnswerOption ".
            "WHERE AnswerOption.qid = Question.qid ".
                "AND Answer.qid = Question.qid ".
                "AND Answer.aid = AnswerOption.aid ".
                "AND Participant.uid = Answer.uid ".
                "AND Participant.uid = ?";
        $result = Database::getInstance()->performPreparedStatement($query, 'i', $this->getUID());
        $answers = [];
        foreach($result as $row) {
            $answers[$row['qid']] = [
                "value" => $row['value'],
                "question" => $row['question']
            ];
        }
        $query = "SELECT Question.qid, question, text " .
            "FROM Question, Participant, Answer ".
            "WHERE Answer.qid = Question.qid ".
                "AND Participant.uid = Answer.uid ".
                "AND text IS NOT NULL ".
                "AND Participant.uid = ?";
        $result = Database::getInstance()->performPreparedStatement($query, 'i', $this->getUID());
        foreach($result as $row) {
            $answers[$row['qid']] = [
                "value" => $row['text'],
                "question" => $row['question']
            ];
        }
        return $answers;
    }

//    public function getAnswers(): array {
//        $db = Database::getInstance();
//        $query = "SELECT qid, aid, text FROM Answer WHERE uid = ?";
//        $result = $db->performPreparedStatement($query, 'i', $this->uid);
//        $answers = [];
//        foreach($result as $answer) {
//            if($answer['aid'] != null) {
//                $a = $answer['aid'];
//            } else {
//                $a = $answer['text'];
//            }
//            if (array_key_exists($answer['qid'], $answers)) {
//                array_push($answers[$answer['qid']], $a);
//            } else {
//                $answers[$answer['qid']] = [$a];
//            }
//        }
//        return $answers;
//    }

    /**
     * Save the answers to the questions provided by the participant.
     * @required All answers should be cleaned!!!
     * @param int[]          $qids    The IDs of the questions that were answered.
     * @param int[]|string[] $answers A mixed list of integers and strings that consist out of answer IDs and text inputs.
     * @return bool                   True on success, false on failure.
     */
    public function saveAnswers(array $qids, array $answers):bool {
        if (count($qids) != count($answers)) {
            return false;
        }
        $values = [
            "TEXT" => [],
            "AID" => []
        ];
        $types = [
            "TEXT" => "",
            "AID" => ""
        ];
        for($i = 0; $i < count($qids); $i++) {
            $qid = $qids[$i];
            $answer = $answers[$i];
            if(is_int($answer)) {
                $types["AID"] .= "iii";
                $type = "AID";
            } else {
                $types["TEXT"] .= "isi";
                $type = "TEXT";
            }
            array_push($values[$type], $this->uid, $answer, $qid);
        }
        if(count($values["AID"]) > 0) {
            $query = "INSERT INTO Answer (uid, aid, qid) VALUES " . str_repeat("(?, ?, ?), ", count($values["AID"]) / 3);
            $query = substr($query, 0, -2);
            Database::getInstance()->performPreparedStatementUpdate($query, $types["AID"], ...$values["AID"]);
        }
        if(count($values["TEXT"]) > 0) {
            $query = "INSERT INTO Answer (uid, text, qid) VALUES " . str_repeat("(?, ?, ?), ", count($values["TEXT"]) / 3);
            $query = substr($query, 0, -2);
            Database::getInstance()->performPreparedStatementUpdate($query, $types["TEXT"], ...$values["TEXT"]);
        }
        return true;
    }

    /**
     * Checks if this participant is already present in the database.
     * If this is the case, we only update the fields. Otherwise, we will create a new record.
     */
    public function save(): void {
        $query = "SELECT * FROM Participant WHERE uid = ?";
        $db = Database::getInstance();
        $result = $db->performPreparedStatement($query, 'i', $this->uid);
        if(count($result) == 0) {
            // Participant doesn't exist yet...
            // FIXME: This query will break...
            $query = "INSERT INTO Participant (uid, hash, progress) VALUES (?, ?, ?)";
            $db->performPreparedStatementUpdate($query, 'isi', $this->uid, $this->hashedUid, $this->progress);
        } else {
            // Participant exists, only update the data.
            $query = "UPDATE Participant SET hash = ?, progress = ? WHERE uid = ?";
            $db->performPreparedStatementUpdate($query, 'sii', $this->hashedUid, $this->progress, $this->uid);
        }
    }

    /**
     * Removes the entire participant and their answers from the database.
     */
    public function remove(): void {
        $query = "DELETE FROM Participant WHERE uid = ?";
        Database::getInstance()->performPreparedStatementUpdate($query, 'i', $this->uid);
    }

    public static function get(int $id): ?Participant {
        $query = "SELECT * FROM Participant WHERE uid = ?";
        $result = Database::getInstance()->performPreparedStatement($query, 'i', $id);
        if (count($result) == 0) {
            return null;
        } else {
            try {
                return new Participant($result[0]['uid'], $result[0]['hash'], $result[0]['progress']);
            } catch (Exception $e) {
                return null;
            }
        }
    }

    public static function getFromHash(string $hash): ?Participant {
        $hash = Participant::filterHash($hash);
        $query = "SELECT * FROM Participant WHERE hash = ?";
        $result = Database::getInstance()->performPreparedStatement($query, 's', $hash);
        if (count($result) == 0) {
            return null;
        } else {
            try {
                return new Participant($result[0]['uid'], $result[0]['hash'], $result[0]['progress']);
            } catch (Exception $e) {
                return null;
            }
        }
    }

    public static function authenticate(string $hash, string $password): bool {
        $hash = Participant::filterHash($hash);
        $password = Participant::filterPassword($password);
        $query = "SELECT * FROM Participant WHERE hash = ?";
        $result = Database::getInstance()->performPreparedStatement($query, 's', $hash);
        if (count($result) == 0) {
            return false;
        } else {
            $password_hash = $result[0]['password'];
            return password_verify($password, $password_hash);
        }
    }

    /**
     * Removes all characters that do not belong to the generic output of a SHA256 hash.
     * @param string $unfiltered_hash The unfiltered hash
     * @return string                 A cleaned hash with only integers, and lower- and uppercase letters.
     */
    public static function filterHash(string $unfiltered_hash): string {
        $pattern = "/[^a-zA-Z0-9]+/";
        return preg_replace($pattern, "", $unfiltered_hash);
    }

    /**
     * Removes all characters that do not belong to a generated password.
     * @param string $unfiltered_password The unfiltered hash
     * @return string                     A cleaned password
     */
    public static function filterPassword(string $unfiltered_password): string {
        $pattern = "/[^a-zA-Z0-9!@#$%^&*()\-_+=<>?]+/";
        return preg_replace($pattern, "", $unfiltered_password);
    }

    /**
     * @return string|null
     */
    public function getPlaintextPassword(): ?string
    {
        return $this->plaintext_password;
    }

    /**
     * @param string|null $plaintext_password
     */
    public function setPlaintextPassword(?string $plaintext_password): void
    {
        $this->plaintext_password = $plaintext_password;
    }
}