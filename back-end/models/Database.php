<?php

require dirname(__DIR__) . '/Credentials.php';

/**
 * Class Database
 * The Database class consists out of a bunch of functions that take of some of the heavy lifting.
 * These functions simplify the interaction with the database and make sure that there is a stable connection.
 *
 * @author Tom Meulenkamp
 */
class Database {

    private mysqli $db;

    private static ?Database $databaseObject = null;

    /**
     * Database constructor.
     */
    private function __construct() {
        $this->db = new mysqli(Credentials::$DATABASE_URL, Credentials::$DATABASE_USERNAME,
            Credentials::$DATABASE_PASSWORD, Credentials::$DATABASE_NAME);
    }

    /**
     * Returns a single instance of the database.
     * @return Database The database object that interacts with the MySQL database.
     */
    static function getInstance(): Database {
        if (Database::$databaseObject == null) {
            Database::$databaseObject = new Database();
        }
        Database::$databaseObject->db->close();
        Database::$databaseObject->db->connect(Credentials::$DATABASE_URL, Credentials::$DATABASE_USERNAME,
            Credentials::$DATABASE_PASSWORD, Credentials::$DATABASE_NAME);
        return Database::$databaseObject;
    }

    /**
     * Performs a query on the database.
     * @param $query string query to perform
     * @return array the result array
     */
    public function performQuery(string $query): array {
        $result = $this->db->query($query);
        $array = array();
        while($row = $result->fetch_assoc()) {
            array_push($array, $row);
        }
        return $array;
    }

    /**
     * Performs a prepared statement on the database.
     * @param $query string     The query to perform
     * @param $types string     A string with all the types of each variable
     *                          i -> integer; d -> doubles; s -> string; b -> blob.
     * @param $variables array  The variables to put in the query.
     * @return array            The result array
     */
    public function performPreparedStatement(string $query, string $types, ...$variables): array {
        $statement = $this->db->prepare($query);
        $statement->bind_param($types, ...$variables);
        $statement->execute();
        $statement->store_result();
        $array = Array();
        while($row = $this->fetchAssocStatement($statement)) {
            array_push($array, $row);
        }
        return $array;
    }

    /**
     * A helper method that replicates the fetch_assoc method that was once provided by the now deprecated version of
     * the PHP mysqli library.
     * @param $stmt mysqli_stmt The statement to be processed.
     * @return array|null If rows are found, return them. If no rows (thus no results) are present, return null.
     */
    function fetchAssocStatement(mysqli_stmt $stmt): ?array {
        if($stmt->num_rows>0)
        {
            $result = array();
            $md = $stmt->result_metadata();
            $params = array();
            while($field = $md->fetch_field()) {
                $params[] = &$result[$field->name];
            }
            call_user_func_array(array($stmt, 'bind_result'), $params);
            if($stmt->fetch())
                return $result;
        }

        return null;
    }

    /**
     * Performs a statement on the database, without feedback.
     * @param $query string query to perform.
     */
    public function performUpdate(string $query): void {
        $this->db->query($query);
    }

    /**
     * Performs a prepared statement on the database, without feedback.
     * @param $query string     The query to perform
     * @param $types string     A string with all the types of each variable
     *                          i -> integer; d -> doubles; s -> string; b -> blob.
     * @param $variables array  The variables to put in the query.
     */
    public function performPreparedStatementUpdate(string $query, string $types, ...$variables): void {
        $statement = $this->db->prepare($query);
        $statement->bind_param($types, ...$variables);
        $statement->execute();
    }
}