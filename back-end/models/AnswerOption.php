<?php

interface AnswerOption
{

    /**
     * @return int Obtains the id of an answer.
     */
    public function getId(): int;

    public function getHTML(): void;

    public function __toString(): string;

}