<?php

require_once dirname(__DIR__) . '/AnswerOption.php';

class CheckBox implements AnswerOption, DatabaseObject
{

    private int $aid;
    private string $value;
    private int $qid;

    public function __construct(int $aid, string $value, int $qid) {
        $this->aid = $aid;
        $this->value = $value;
        $this->qid = $qid;
    }

    /**
     * @inheritDoc
     */
    public function getId(): int {
        return $this->aid;
    }

    public function getHTML(): void {
        $template_path = dirname(__DIR__, 3) . '/front-end/templates/checkBox.php';
        require $template_path;
    }

    public function save(): void {
        throw new InvalidArgumentException("Not supported, please enter this directly into the database.");
    }

    public static function get(int $id): ?CheckBox {
        $db = Database::getInstance();
        $query = "SELECT value, type, qid FROM AnswerOption WHERE aid = ?";
        $result = $db->performPreparedStatement($query, 'i', $id);
        if (count($result) == 0 || Question::$ANSWER_TYPES[$result[0]['type']] != "CHECKBOX") {
            return null;
        } else {
            return new CheckBox($id, $result[0]['value'], $result[0]['qid']);
        }
    }

    public function __toString(): string {
        return "Check box: " . $this->value;
    }
}