<?php

class TextField implements AnswerOption
{
    /**
     * The id of the question that is related to this answer.
     * @var int
     */
    private int $qid;

    public function __construct($qid) {
        $this->qid = $qid;
    }

    public function getId(): int {
        return $this->qid;
    }

    public function getHTML(): void {
        $template_path = dirname(__DIR__, 3) . '/front-end/templates/textField.php';
        require $template_path;
    }

    public function __toString(): string {
        return "Text Field";
    }

}