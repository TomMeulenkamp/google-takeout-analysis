<?php

require_once dirname(__DIR__) . '/AnswerOption.php';

class RadioButton implements AnswerOption, DatabaseObject
{

    private int $qid;
    private int $aid;
    private string $value;

    /**
     * Creates a RadioButton model (the data should already be in the database)
     * @param $aid   int    The database ID of this button.
     * @param $value string The value of this button.
     * @param $qid   int    The ID is the question this button is related to.
     */
    public function __construct(int $aid, string $value, int $qid) {
        $this->aid = $aid;
        $this->value = $value;
        $this->qid = $qid;
    }

    /**
     * @inheritDoc
     */
    public function getId(): int {
        return $this->aid;
    }

    public function getHTML(): void {
        $template_path = dirname(__DIR__, 3) . '/front-end/templates/radioButton.php';
        require $template_path;
    }

    public function save(): void {
        throw new InvalidArgumentException("Not supported, please enter this directly into the database.");
    }

    public static function get(int $id): ?RadioButton {
        $db = Database::getInstance();
        $query = "SELECT value, type, qid FROM AnswerOption WHERE aid = ?";
        $result = $db->performPreparedStatement($query, 'i', $id);
        if (count($result) == 0) {
            return null;
        }
        $type = Question::$ANSWER_TYPES[$result[0]['type']];
        if ($type != "RADIO" && $type != "LIKERT") {
            return null;
        } else {
            return new RadioButton($id, $result[0]['value'], $result[0]['qid']);
        }
    }

    public function __toString(): string {
        return "Radio Button: " . $this->value;
    }
}