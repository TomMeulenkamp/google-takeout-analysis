<?php

require_once __DIR__ . "/Question.php";

class LikertQuestion extends Question {

    public function getHTML(): void {
        $template_path = dirname(__DIR__, 2) . '/front-end/templates/likert_question.php';
        include $template_path;
    }

    public static function get(int $qid): ?LikertQuestion {
        $q = parent::get($qid);
        if ($q == null) {
            return null;
        } else {
            return new LikertQuestion($q->getId(), $q->getQuestion(), $q->getAnswerOptions());
        }
    }

}