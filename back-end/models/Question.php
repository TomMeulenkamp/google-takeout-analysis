<?php

require_once "DatabaseObject.php";
require_once "Database.php";
require_once "AnswerOption.php";
require_once __DIR__ . '/answerOptions/CheckBox.php';
require_once __DIR__ . '/answerOptions/RadioButton.php';
require_once __DIR__ . '/answerOptions/TextField.php';

/**
 * The question database model.
 */
class Question implements DatabaseObject
{
    public static array $ANSWER_TYPES = [
        "TEXT" => 0,
        "RADIO" => 1,
        "CHECKBOX" => 2,
        "LIKERT" => 3,
        0 => "TEXT",
        1 => "RADIO",
        2 => "CHECKBOX",
        3 => "LIKERT"
    ];

    private int $qid;
    private string $question;
    private array $answerOptions;

    /**
     * Creates a new question database object.
     * @param int $qid                      The database ID of this question.
     * @param string $question              The question in plain-text.
     * @param AnswerOption[] $answerOptions The answer options that the participant can enter.
     */
    public function __construct(int $qid, string $question, array $answerOptions) {
        $this->question = $question;
        $this->answerOptions = $answerOptions;
        $this->qid = $qid;
    }

    public function getId(): int {
        return $this->qid;
    }

    /**
     * Retrieve the plain-text question.
     * @return string
     */
    public function getQuestion(): string
    {
        return $this->question;
    }

    /**
     * Retrieve the possible options to the answer. This can be text-input, radio buttons, or checkboxes.
     * @return AnswerOption[]
     */
    public function getAnswerOptions(): array
    {
        return $this->answerOptions;
    }

    /**
     * TODO: Should multiple answers, answered by a single user be grouped or just treated as separate answers?
     * Retrieve all anonymous answers that are currently stored in the database.
     * The answers are either returned as an array of answer IDs or as an array of string with text input.
     * @return array
     */
    public function getAnswers(): array
    {
        $db = Database::getInstance();
        $query = "SELECT text, aid FROM Answer WHERE qid = ?";
        $result = $db->performPreparedStatement($query, 'i', $this->qid);
        $answers = array();
        foreach($result as $answer) {
            if($answer['aid'] != null) {
                array_push($answers, $answer['aid']);
            } else {
                array_push($answers, $answer['text']);
            }
        }
        return $answers;
    }

    public function getHTML(): void {
        $template_path = dirname(__DIR__, 2) . '/front-end/templates/question.php';
        include $template_path;
    }

    public function save(): void {
        throw new InvalidArgumentException("Not supported, please enter this directly into the database.");
    }

    public static function get(int $qid): ?Question {
        $db = Database::getInstance();
        $query = "SELECT question FROM Question WHERE qid = ?";
        $result = $db->performPreparedStatement($query, 'i', $qid);
        if (count($result) == 0) {
            return null;
        } else {
            $question = $result[0]['question'];
            $answerOptions = [];
            $query = "SELECT aid, type FROM AnswerOption WHERE qid = ?";
            $res = $db->performPreparedStatement($query, 'i', $qid);
            foreach ($res as $answer) {
                $id = $answer['aid'];
                switch (Question::$ANSWER_TYPES[$answer['type']]) {
                    case "TEXT":
                        array_push($answerOptions, new TextField($id));
                        break;
                    case "RADIO":
                    case "LIKERT":
                        array_push($answerOptions, RadioButton::get($id));
                        break;
                    case "CHECKBOX":
                        array_push($answerOptions, CheckBox::get($id));
                        break;
                }
            }
            return new Question($qid, $question, $answerOptions);
        }
    }

    public function __toString(): string {
        $string = "Question: " . $this->question . "<br />Answer Options:<br />";
        foreach($this->answerOptions as $answerOption) {
            $string .= $answerOption . "<br />";
        }
        return $string;
    }

}