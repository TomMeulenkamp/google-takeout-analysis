<?php

require_once __DIR__ . "/Question.php";

class OpenQuestion extends Question {

    public function getHTML(): void {
        $template_path = dirname(__DIR__, 2) . '/front-end/templates/open_question.php';
        include $template_path;
    }

    public static function get(int $qid): ?OpenQuestion {
        $q = parent::get($qid);
        if ($q == null) {
            return null;
        } else {
            return new OpenQuestion($q->getId(), $q->getQuestion(), $q->getAnswerOptions());
        }
    }

}