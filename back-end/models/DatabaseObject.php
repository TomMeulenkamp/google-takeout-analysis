<?php

/**
 * Each object that is stored in the database should have the ability to be obtained and saved. This interface
 * should be applied to each object, such that functions are clear.
 */
interface DatabaseObject
{
    /**
     * Stores the object in the database.
     */
    public function save(): void;

    /**
     * Searches for the object in the database.
     * @param int $id The PK of the object
     * @return DatabaseObject|null The object if it's present and false otherwise.
     */
    public static function get(int $id) :?DatabaseObject;
}