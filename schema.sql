DROP TABLE IF EXISTS Answer;
DROP TABLE IF EXISTS CollectedData;
DROP TABLE IF EXISTS AnswerOption;
DROP TABLE IF EXISTS Question;
DROP TABLE IF EXISTS Participant;

CREATE TABLE Participant (
    uid INTEGER UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    hash VARCHAR(15) NOT NULL,
    progress INTEGER NOT NULL,
    password TEXT NOT NULL
);

CREATE TABLE Question (
    qid INTEGER UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    question TEXT NOT NULL
);

-- The options that are available for each question.
CREATE TABLE AnswerOption (
    aid INTEGER UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    qid INTEGER UNSIGNED NOT NULL,
    value TEXT NOT NULL, -- The value of an answer
    type INTEGER UNSIGNED NOT NULL,
    FOREIGN KEY (qid) REFERENCES Question(qid) ON DELETE CASCADE
);

-- If an answer is provided, there should either be an answer linked from the database or the text field should be filled.
CREATE TABLE Answer (
    uid INTEGER UNSIGNED NOT NULL,
    aid INTEGER UNSIGNED DEFAULT NULL,
    text TEXT DEFAULT NULL,
    qid INTEGER UNSIGNED NOT NULL,
    not_null TEXT GENERATED ALWAYS AS (coalesce(aid, text)) VIRTUAL NOT NULL, -- Enforces above rule
    FOREIGN KEY (uid) REFERENCES Participant(uid) ON DELETE CASCADE,
    FOREIGN KEY (qid) REFERENCES Question(qid),
    FOREIGN KEY (aid) REFERENCES AnswerOption(aid)
);