const revoke_button = document.getElementById('revoke_button');
const revoke_popup = document.getElementById('revoke_popup');
const close_popup = document.getElementById('return_button');
const continue_button = document.getElementById('continue_button');

revoke_button.addEventListener('click', function () {
    revoke_popup.style.display = 'flex';
});

close_popup.addEventListener('click', function () {
    revoke_popup.style.display = 'none';
});

continue_button.addEventListener('click', function () {
    window.location.replace('/participant/revoke');
})