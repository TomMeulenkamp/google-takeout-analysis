const elem = document.getElementById('check');
const button = document.getElementsByTagName('button')[0];
const next = document.getElementsByTagName('a')[0];

next.setAttribute("href", "#");
next.style.cursor = "not-allowed";
button.style.cursor = "not-allowed";

elem.addEventListener('change', function (event) {
    let target = event.target;
    if(target.checked) {
        next.setAttribute("href", "/pre-analysis");
        next.style.cursor = "pointer";
        button.style.cursor = "pointer";
    } else {
        next.setAttribute("href", "#");
        next.style.cursor = "not-allowed";
        button.style.cursor = "not-allowed";
    }
});