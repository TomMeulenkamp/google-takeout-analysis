const fileInput = document.getElementById('input');
const loader = document.getElementById('loader');
const finished = document.getElementById('finished');

fileInput.addEventListener('change', async (event) => {
    loader.style.display = 'flex';
    
    let zipfs = new zip.fs.FS(); // create new zip filesystem
    await zipfs.importBlob(event.target.files[0]); // wait for zip to be imported

    await browser_history(zipfs);
    await youtube_history(zipfs);
    await youtube_watches(zipfs);
    await youtube_comments(zipfs);
    await youtube_channel(zipfs);
    await google_play_purchase_history(zipfs);
    await google_play_store_reviews(zipfs);
    await user_devices(zipfs);
    await user_activity(zipfs);
    await create_location_map(zipfs);
    
    await loader.remove();
    finished.style.display = 'block';
});

/**
 * Removes all the junk after the / in a domain.
 * @param {String} url The URL to filter
 * @returns A clean domain name only
 */
function filter_domain(url) {
    url = url.replaceAll('https://', '');
    url = url.replaceAll('http://', '');
    url = url.replaceAll('www.', '');
    url = url.replaceAll(/\/(.*)/g, '');
    return url;
}

/**
 * Creates a new section on the page which can display the message that particular data isn't found.
 * @param id        The id to add to this particular statistics block
 * @param title     The title of this block
 * @param message   The message in this block
 */
function no_data_found(id, title, message) {
    let browser_history_element = document.getElementById('center');
    let div_outer = document.createElement("div");
    div_outer.classList.add("statistic_block");
    let div_inner_stats = document.createElement("div");
    div_inner_stats.setAttribute("id", id);
    let title_elem = document.createElement('h2');
    let message_elem = document.createElement('p');
    title_elem.innerText = title;
    message_elem.innerText = message;
    div_inner_stats.appendChild(title_elem);
    div_inner_stats.appendChild(message_elem);
    div_outer.appendChild(div_inner_stats);
    browser_history_element.appendChild(div_outer);
}

// function to check the browser history
async function browser_history(zipfs) {
    try {
        let file = zipfs.find("Takeout/Chrome/BrowserHistory.json"); // find the responsible file for the analysis
        let fileData = await file.getText(); // gets the text stored in the file.

        // get the browser history data
        let browser_history = JSON.parse(fileData);
        // get the field of browser history (because of the structure of the json file)
        browser_history = browser_history["Browser History"]

        // in case the months will be displayed, we store as key particular month and as value
        // the number of searches
        let browser_history_months = {}
        // in case the years will be displayed, we store as key particular month and as value
        // the number of searches
        let browser_history_years = {}

        // if the monthly statistics will be displayed, then we show all the 12 months
        for (let i = 0; i < 12; i++) {
            browser_history_months[i] = 0
        }

        // Define variables for time interval
        // and dictionary for visited websites
        AllWebsites = {}
        let min_time = 0
        let max_time = 0

        // iterate through all the data
        for (let i = 0; i < browser_history.length; i++) {
            let item = browser_history[i];
            let website = filter_domain(item.url);

            // count websites visits
            if (website in AllWebsites) {
                AllWebsites[website]["counter"] += 1
            } else if (item.favicon_url !== undefined) {
                AllWebsites[website] = {
                    "counter": 1,
                    "favicon": item.favicon_url
                }
            } else {
                AllWebsites[website] = {
                    "counter": 1,
                    "favicon": "#"
                }
            }

            // finding out the time interval
            if (min_time === 0 || min_time > item.time_usec) {
                min_time = item.time_usec;
            }
            if (max_time === 0 || max_time < item.time_usec) {
                max_time = item.time_usec;
            }

            // get the date in right format of the particular record
            let date = new Date(item.time_usec / 1000);
            // stores how many browser records happened in specific months
            if (date.getMonth() in browser_history_months) {
                browser_history_months[date.getMonth()] += 1
            }
            // store how many records happened in specific years
            if (date.getFullYear() in browser_history_years) {
                browser_history_years[date.getFullYear()] += 1
            } else {
                browser_history_years[date.getFullYear()] = 1
            }
            
        }

        // Sort websites by number of visits
        var items = Object.keys(AllWebsites).map(function(key) {
            return [key, AllWebsites[key]];
          });
        items.sort(function(first, second) {
            return second[1]["counter"] - first[1]["counter"];
        });

        // transform the min date to a proper format
        let min_date = new Date(min_time / 1000);
        let month_min = min_date.getUTCMonth() + 1;
        let day_min = min_date.getUTCDate();
        let year_min = min_date.getUTCFullYear();
        let newdate_min = day_min + "/" + month_min + "/" + year_min;
        
        // transform the min date to a proper format
        let max_date= new Date(max_time / 1000);
        let month_max = max_date.getUTCMonth() + 1;
        let day_max = max_date.getUTCDate();
        let year_max = max_date.getUTCFullYear();
        let newdate_max = day_max + "/" + month_max + "/" + year_max;

        let main_elem = document.getElementById('center');
        let services_history = document.createElement("div");
        let services_history_title = document.createElement("h2");
        services_history_title.innerText = "Top 20 visited pages";
        services_history.appendChild(services_history_title);
        services_history.classList.add("statistic_block");
        services_history.setAttribute("id", "visited_websites");
        let services_history_paragraph = document.createElement("p");
        services_history_paragraph.innerHTML = "Your top 20 visited websites visited from " + newdate_min + " till " + newdate_max + " are:";
        services_history.appendChild(services_history_paragraph);
        
        let services_history_table = document.createElement("table");
        services_history_table.style.margin = 'auto';
        services_history_table.innerHTML = "<tr><th>Website</th><th>Number of times visited</th></tr>";
        for (let k = 1; k < 21; k++) {
            services_history_table.innerHTML += "<tr><td>" + items[k][0] + "</td><td>" + items[k][1]["counter"] + "</td></tr>";
        }
        services_history_table.innerHTML += "</table>";
        services_history.appendChild(services_history_table);
        main_elem.appendChild(services_history)

        if (browser_history.length !== 0) {

            // display the total number of visited pages
            let browser_history_element = document.getElementById('center');

            let div_outer = document.createElement("div");
            div_outer.classList.add("statistic_block")
            let div_inner_stats = document.createElement("div")
            div_inner_stats.setAttribute("id", "browser_history")
            div_outer.appendChild(div_inner_stats);
            let div_inner_map = document.createElement("div")
            div_outer.appendChild(div_inner_map)
            let inner_map = document.createElement("canvas")
            inner_map.setAttribute("id", "BrowserHistoryChart")
            div_inner_map.appendChild(inner_map)
            browser_history_element.appendChild(div_outer)

            div_inner_stats.innerHTML = "<h2>Browser History</h2><p> Google has collected browser data from you. Number of visited web pages: " + browser_history.length + "</p>";

            // if the browser history is only in one month, then show the data within that year, otherwise show different years
            let labels;
            // the label to display on top of the graph
            let graph_header;
            // the data to fill the graph with
            let data_graph = [];
            // data within on year
            if (Object.keys(browser_history_years).length === 1) {
                labels = [
                    'January',
                    'February',
                    'March',
                    'April',
                    'May',
                    'June',
                    'July',
                    'August',
                    'September',
                    'October',
                    'November',
                    'December'
                ];
                graph_header = 'The amount of browser searches tracked by Google in ' + new Date(browser_history[0].time_usec / 1000).getFullYear().toString();

                for (let i = 0; i < 12; i++) {
                    data_graph.push(browser_history_months[i]);
                }
            }
            // data not within one year
            else {
                graph_header = 'The amount of browser searches saved by Google';
                labels = []
                // take keyset of years
                const keys = Object.keys(browser_history_years);
                for (let i = 0; i < keys.length; i++) {
                    labels.push(keys[i]);
                    data_graph.push(browser_history_years[keys[i]])
                }
            }

            // plot the data as a bar chart
            const data = {
                labels: labels,
                datasets: [{
                    label: graph_header,
                    backgroundColor: '#11999e',
                    borderColor: '#11999e',
                    data: data_graph,
                }]
            };
            const config = {
                type: 'bar',
                data: data,
                options: {
                    scales: {
                        y: {
                            beginAtZero: true
                        }
                    }
                },
            };
            let myChart = new Chart(
                document.getElementById('BrowserHistoryChart'),
                config
            );
            
        } else {
            no_data_found('browser_history', 'Browser History', 'There was no browser history found in your data.');
            
        }
    } catch (error) {
        
        no_data_found('browser_history', 'Browser History', 'There was no browser history found in your data.');
    }
}

// function to check the youtube history
async function youtube_history(zipfs) {
    try {
        let file = zipfs.find("Takeout/YouTube and YouTube Music/history/search-history.html") // find the responsible file for the analysis
        let fileData = await file.getText() // gets the text stored in the file.

        // parser for the html content
        let parser = new DOMParser();
        let html = parser.parseFromString(fileData, 'text/html');

        // in case the months will be displayed, we store as key particular month and as value
        // the number of searches
        let youtube_history_months = {}
        // in case the years will be displayed, we store as key particular month and as value
        // the number of searches
        let youtube_history_years = {}

        // if the monthly statistics will be displayed, then we show all the 12 months
        for (let i = 0; i < 12; i++) {
            youtube_history_months[i] = 0
        }

        // get the needed information of specific entry
        let elements = html.getElementsByClassName("content-cell mdl-cell mdl-cell--6-col mdl-typography--body-1");
        for (let i = 0, len = elements.length; i < len; i++) {
            // skip the class if it has empty text, those are classed with odd numbers
            if (i % 2 === 0) {
                // get the date in right format of the particular record
                // get year. For example, 2021
                let date_helper = elements[i].innerHTML.split('<br>');
                date_helper = date_helper[date_helper.length-1].split(',');

                let date = date_helper[0]

                for (let j=1; j < date_helper.length - 1; j++) {
                    date += date_helper[j]
                }

                date = new Date(date)

                let year = date.getFullYear();
                let month = date.getMonth();

                // stores how many youtube records happened in specific months
                if (month in youtube_history_months) {
                    youtube_history_months[month] += 1
                }
                // store how many records happened in specific years
                if (year in youtube_history_years) {
                    youtube_history_years[year] += 1
                } else {
                    youtube_history_years[year] = 1
                }
            }
        }

        if (elements.length !== 0) {

            // display the total number of visited youtube searches
            // display the total number of visited pages
            let youtube_history_element = document.getElementById('center');

            let div_outer = document.createElement("div");
            div_outer.classList.add("statistic_block")
            div_outer.setAttribute("id", "youtube")
            let div_inner_stats = document.createElement("div")
            div_inner_stats.setAttribute("id", "youtube_history")
            div_outer.appendChild(div_inner_stats);
            let div_inner_map = document.createElement("div")
            div_outer.appendChild(div_inner_map)
            let inner_map = document.createElement("canvas")
            inner_map.setAttribute("id", "YoutubeHistoryChart")
            div_inner_map.appendChild(inner_map)
            youtube_history_element.appendChild(div_outer)

            div_inner_stats.innerHTML = "<p><h2>YouTube History</h2>Google has collected search data from YouTube. Number of searched YouTube videos: " + (elements.length / 2) + "</p>";

            // if the youtube history is only in one month, then show the data within that year, otherwise show different years
            let labels;
            // the label to display on top of the graph
            let graph_header;
            // the data to fill the graph with
            let data_graph = [];
            // data within on year
            if (Object.keys(youtube_history_years).length === 1) {
                labels = [
                    'January',
                    'February',
                    'March',
                    'April',
                    'May',
                    'June',
                    'July',
                    'August',
                    'September',
                    'October',
                    'November',
                    'December'
                ];
                let date_helper = elements[0].innerHTML.split('<br>');
                date_helper = date_helper[date_helper.length-1].split(',');

                let date = date_helper[0]

                for (let j=1; j < date_helper.length - 1; j++) {
                    date += date_helper[j]
                }

                date = new Date(date)

                let year = date.getFullYear();
                graph_header = 'The amount of YouTube search data tracked by Google in ' + year;

                for (let i = 0; i < 12; i++) {
                    data_graph.push(youtube_history_months[i]);
                }
            }
            // data not within one year
            else {
                graph_header = 'The amount of YouTube search data tracked by Google';
                labels = []
                // take keyset of years
                const keys = Object.keys(youtube_history_years);
                for (let i = 0; i < keys.length; i++) {
                    labels.push(keys[i]);
                    data_graph.push(youtube_history_years[keys[i]])
                }
            }

            // plot the data as a bar chart
            const data = {
                labels: labels,
                datasets: [{
                    label: graph_header,
                    backgroundColor: '#11999e',
                    borderColor: '#11999e',
                    data: data_graph,
                }]
            };
            const config = {
                type: 'bar',
                data: data,
                options: {
                    scales: {
                        y: {
                            beginAtZero: true
                        }
                    }
                },
            };
            let myChart = new Chart(
                document.getElementById('YoutubeHistoryChart'),
                config
            );
            
        } else {
            
            no_data_found('youtube_history', 'YouTube History', 'There was no YouTube history found in your data.');
        }
    } catch (error) {
        
        no_data_found('youtube_history', 'YouTube History', 'There was no YouTube history found in your data.');
        console.log(error)
    }
}

// function to check the youtube watch history
async function youtube_watches(zipfs) {
    try {
        let file = zipfs.find("Takeout/YouTube and YouTube Music/history/watch-history.html") // find the responsible file for the analysis
        let fileData = await file.getText() // gets the text stored in the file.

        // parser for the html content
        let parser = new DOMParser();
        let html = parser.parseFromString(fileData, 'text/html');

        // in case the months will be displayed, we store as key particular month and as value
        // the number of watches
        let youtube_history_months = {}
        // in case the years will be displayed, we store as key particular month and as value
        // the number of searches
        let youtube_history_years = {}

        // if the monthly statistics will be displayed, then we show all the 12 months
        for (let i = 0; i < 12; i++) {
            youtube_history_months[i] = 0
        }

        // get the needed information of specific entry
        let elements = html.getElementsByClassName("content-cell mdl-cell mdl-cell--6-col mdl-typography--body-1");
        for (let i = 0, len = elements.length; i < len; i++) {
            // skip the class if it has empty text, those are classed with odd numbers
            if (i % 2 === 0) {
                // get the date in right format of the particular record
                // get year. For example, 2021
                let date_helper = elements[i].innerHTML.split('<br>');
                date_helper = date_helper[date_helper.length-1].split(',');

                let date = date_helper[0]

                for (let j=1; j < date_helper.length - 1; j++) {
                    date += date_helper[j]
                }

                date = new Date(date)

                let year = date.getFullYear();
                let month = date.getMonth();

                // stores how many youtube records happened in specific months
                if (month in youtube_history_months) {
                    youtube_history_months[month] += 1
                }
                // store how many records happened in specific years
                if (year in youtube_history_years) {
                    youtube_history_years[year] += 1
                } else {
                    youtube_history_years[year] = 1
                }
            }
        }

        if (elements.length !== 0) {

            // display the total number of visited youtube searches
            // display the total number of visited pages

            let youtube_watches_element = document.getElementById('youtube');

            if(typeof youtube_watches_element === 'undefined') {
                let main_elem = document.getElementById('center')
                youtube_watches_element = document.createElement("div");
                youtube_watches_element.classList.add("statistic_block")
                youtube_watches_element.setAttribute("id", "youtube")
                main_elem.appendChild(youtube_watches_element)
            }

            let div_inner_stats = document.createElement("div")
            div_inner_stats.setAttribute("id", "youtube_watches")
            youtube_watches_element.appendChild(div_inner_stats);
            let div_inner_map = document.createElement("div")
            youtube_watches_element.appendChild(div_inner_map)
            let inner_map = document.createElement("canvas")
            inner_map.setAttribute("id", "YoutubeWatchesChart")
            div_inner_map.appendChild(inner_map)

            div_inner_stats.innerHTML = "<p><h2>YouTube Views</h2>Google has collected information about the videos you watched in YouTube. Number of watched YouTube videos: " + (elements.length / 2) + "</p>";

            // if the youtube watches is only in one month, then show the data within that year, otherwise show different years
            let labels;
            // the label to display on top of the graph
            let graph_header;
            // the data to fill the graph with
            let data_graph = [];
            // data within on year
            if (Object.keys(youtube_history_years).length === 1) {
                labels = [
                    'January',
                    'February',
                    'March',
                    'April',
                    'May',
                    'June',
                    'July',
                    'August',
                    'September',
                    'October',
                    'November',
                    'December'
                ];
                graph_header = 'The amount of YouTube views in tracked months saved by Google';

                for (let i = 0; i < 12; i++) {
                    data_graph.push(youtube_history_months[i]);
                }
            }
            // data not within one year
            else {
                graph_header = 'The amount of YouTube views in tracked years saved by Google';
                labels = []
                // take keyset of years
                const keys = Object.keys(youtube_history_years);
                for (let i = 0; i < keys.length; i++) {
                    labels.push(keys[i]);
                    data_graph.push(youtube_history_years[keys[i]])
                }
            }

            // plot the data as a bar chart
            const data = {
                labels: labels,
                datasets: [{
                    label: graph_header,
                    backgroundColor: '#11999e',
                    borderColor: '#11999e',
                    data: data_graph,
                }]
            };
            const config = {
                type: 'bar',
                data: data,
                options: {
                    scales: {
                        y: {
                            beginAtZero: true
                        }
                    }
                },
            };
            let myChart = new Chart(
                document.getElementById('YoutubeWatchesChart'),
                config
            );
            
        } else {
            
            no_data_found('youtube', 'YouTube Views', 'There was no YouTube view history found in your data.');
        }
    } catch (error) {
        
        no_data_found('youtube', 'YouTube Views', 'There was no YouTube view history found in your data.');
        console.log(error)
    }
}

async function youtube_channel(zipfs) {
    try {
        let file = zipfs.find("Takeout/YouTube and YouTube Music/history/watch-history.html") // find the responsible file for the analysis
        let fileData = await file.getText() // gets the text stored in the file.

        // parser for the html content
        let parser = new DOMParser();
        let html = parser.parseFromString(fileData, 'text/html');
        
        let youtube_channels = {}

        let elements = html.getElementsByClassName("content-cell mdl-cell mdl-cell--6-col mdl-typography--body-1");
        if(elements.length > 0) {

            for (let i = 0, len = elements.length; i < len; i++) {
                if (i % 2 === 0) {
                    let channel_helper = elements[i].innerHTML.split('<br>');
                    if (channel_helper.length === 3) {
                        let channel = channel_helper[1].split()
                        let channel_link = document.createElement('html');
                        channel_link.innerHTML = channel;

                        if (channel_link.children[1].children[0] !== undefined) {
                            let channel_text = channel_link.children[1].children[0].innerText

                            if (channel_text in youtube_channels) {
                                youtube_channels[channel_text] += 1
                            } else {
                                youtube_channels[channel_text] = 1
                            }
                        }
                    }
                }
            }

            // Create items array
            var items = Object.keys(youtube_channels).map(function (key) {
                return [key, youtube_channels[key]];
            });

            // Sort the array based on the second element
            items.sort(function (first, second) {
                return second[1] - first[1];
            });

            // Create a new array with only the first 5 items
            console.log(items.slice(0, 10));

            let top_10 = items.slice(0, 10)

            labels = []
            data_graph = []

            for (i in top_10) {
                i = top_10[i][0]
                labels.push(i)
            }
            console.log(labels)

            for (j in top_10) {
                j = top_10[j][1]
                data_graph.push(j)
            }
            console.log(data_graph)


            const data = {
                labels: labels,
                datasets: [{
                    label: "Number of times you have watched these YouTube channels",
                    backgroundColor: '#11999e',
                    borderColor: '#11999e',
                    data: data_graph,
                }]
            };
            const config = {
                type: 'bar',
                data: data,
                options: {
                    scales: {
                        y: {
                            beginAtZero: true
                        }
                    }
                },
            };

            let youtube_channel_div = document.createElement('div');
            youtube_channel_div.classList.add('statistic_block')
            let title_elem = document.createElement('h2');
            title_elem.innerText = 'YouTube Channels';
            youtube_channel_div.appendChild(title_elem);
            let div_inner_map = document.createElement('div');
            youtube_channel_div.appendChild(div_inner_map);
            let inner_map = document.createElement('canvas');
            inner_map.setAttribute('id', 'YouTube_Channel_Chart');
            div_inner_map.appendChild(inner_map);
            document.getElementById('center').appendChild(youtube_channel_div);

            let myChart = new Chart(
                document.getElementById('YouTube_Channel_Chart'),
                config
            );
            
        } else {
            
            no_data_found('youtube_channels', 'YouTube Channels', 'There was no information about your watched channels found in your data.')
        }
    } catch (error) {
        
        no_data_found('youtube_channels', 'YouTube Channels', 'There was no information about your watched channels found in your data.');
        console.log(error)
    }
}

// function to check the number of comments made on youtube
async function youtube_comments(zipfs) {
    try {
        let file = zipfs.find("Takeout/YouTube and YouTube Music/my-comments/my-comments.html") // find the responsible file for the analysis
        let fileData = await file.getText() // gets the text stored in the file.

        // parser for the html content
        let parser = new DOMParser();
        let html = parser.parseFromString(fileData, 'text/html');

        let elements = html.getElementsByTagName("li");

        // display the total number of visited youtube watches

        let youtube_watches_element = document.getElementById('youtube');

        if(typeof youtube_watches_element === 'undefined') {
            let main_elem = document.getElementById('center')
            youtube_watches_element = document.createElement("div");
            youtube_watches_element.classList.add("statistic_block")
            youtube_watches_element.setAttribute("id", "youtube")
            main_elem.appendChild(youtube_watches_element)
        }

        let div_inner = document.createElement("div");
        div_inner.classList.add("youtube_comments");

        div_inner.innerHTML = "<p><h2>YouTube Comments</h2>Number of comments you left on YouTube: " + elements.length + "</p>"
        youtube_watches_element.appendChild(div_inner);
        
    } catch (error) {
        
        no_data_found('youtube_comments', 'YouTube Comments', 'There were no YouTube comments found in your data.');
    }
}

async function google_play_purchase_history(zipfs) {
    try {
        let file = zipfs.find("Takeout/Google Play Store/Purchase History.json"); // find the responsible file for the analysis
        let fileData = await file.getText(); // gets the text stored in the file.

        // get the purchase history data
        let purchase_history = JSON.parse(fileData);
        // in case the months will be displayed, we store as key particular month and as value
        // the number of purchases
        let google_play_history_months = {};
        // in case the years will be displayed, we store as key particular month and as value
        // the number of searches
        let google_play_history_years = {};

        // if the monthly statistics will be displayed, then we show all the 12 months
        for (let i = 0; i < 12; i++) {
            google_play_history_months[i] = 0;
        }

        // variable to count how money was spent
        let money_spent = 0;
        let money_symbol = purchase_history[0]["purchaseHistory"]["invoicePrice"][0];

        // iterate through all the data
        for (let i = 0; i < purchase_history.length; i++) {
            // get the date in right format of the particular record
            let purchase_history_entry = purchase_history[i]["purchaseHistory"];
            // add to spent money the price of purchase
            let re = new RegExp('[0-9]+[.][0-9][0-9]');
            money_spent += parseFloat(re.exec(purchase_history_entry["invoicePrice"].replace(',', '.'))[0]);
            // get the purchase date
            let purchase_time = new Date(purchase_history_entry["purchaseTime"]);
            let purchase_month = purchase_time.getMonth();
            let purchase_year = purchase_time.getFullYear();

            // stores how many records happened in specific months
            if (purchase_month in google_play_history_months) {
                google_play_history_months[purchase_month] += 1;
            }
            // store how many records happened in specific years
            if (purchase_year in google_play_history_years) {
                google_play_history_years[purchase_year] += 1;
            } else {
                google_play_history_years[purchase_year] = 1;
            }
        }

        // display the total number of purchases


        if (purchase_history.length !== 0) {
            let browser_history_element = document.getElementById('center');

            let div_outer = document.createElement("div");
            div_outer.setAttribute("id", "google_play")
            div_outer.classList.add("statistic_block")
            let div_inner_stats = document.createElement("div")
            div_inner_stats.setAttribute("id", "google_play_purchase_history")
            div_inner_stats.innerHTML = "<p><h2>Google Play Purchase History</h2>Number of purchases in Google Play: " + purchase_history.length + ", money spent: " + money_symbol + money_spent + "</p>";
            div_outer.appendChild(div_inner_stats);
            let div_inner_map = document.createElement("div")
            div_outer.appendChild(div_inner_map)
            let inner_map = document.createElement("canvas")
            inner_map.setAttribute("id", "GooglePlayPurchaseChart")
            div_inner_map.appendChild(inner_map)
            browser_history_element.appendChild(div_outer)

            // if the puechases history is only in one month, then show the data within that year, otherwise show different years
            let labels;
            // the label to display on top of the graph
            let graph_header;
            // the data to fill the graph with
            let data_graph = [];
            // data within on year
            if (Object.keys(google_play_history_years).length === 1) {
                labels = [
                    'January',
                    'February',
                    'March',
                    'April',
                    'May',
                    'June',
                    'July',
                    'August',
                    'September',
                    'October',
                    'November',
                    'December'
                ];
                graph_header = 'The amount of purchases tracked by Google in ' + new Date(purchase_history[0]["purchaseHistory"]["purchaseTime"]).getFullYear();

                for (let i = 0; i < 12; i++) {
                    data_graph.push(google_play_history_months[i]);
                }
            }
            // data not within one year
            else {
                graph_header = 'The amount of purchases tracked by Google';
                labels = []
                // take keyset of years
                const keys = Object.keys(google_play_history_years);
                for (let i = 0; i < keys.length; i++) {
                    labels.push(keys[i]);
                    data_graph.push(google_play_history_years[keys[i]])
                }
            }

            // plot the data as a bar chart
            const data = {
                labels: labels,
                datasets: [{
                    label: graph_header,
                    backgroundColor: '#11999e',
                    borderColor: '#11999e',
                    data: data_graph,
                }]
            };
            const config = {
                type: 'bar',
                data: data,
                options: {
                    scales: {
                        y: {
                            beginAtZero: true
                        }
                    }
                },
            };
            let myChart = new Chart(
                document.getElementById('GooglePlayPurchaseChart'),
                config
            );
            
        } else {
            no_data_found('google_play_purchase_history', 'Google Play Purchase History', 'There was no purchase history found in your data.');
            
        }
    } catch (error) {
        
        no_data_found('google_play_purchase_history', 'Google Play Purchase History', 'There was no purchase history found in your data.');
    }
}

async function user_devices(zipfs) {
    try {
        let file = zipfs.find("Takeout/Access Log Activity/Devices - A list of devices (i.e. Nest, Pixel, iPh.csv"); // find the responsible file for the analysis
        let fileData = await file.getText(); // gets the text stored in the file.
        let devices = parse_csv(fileData); // parse the csv text file to a map.

        // add the information found to the webpage
        let main_center = document.getElementById('center');
        let div_outer = document.createElement("div");
        div_outer.classList.add("statistic_block")
        div_outer.setAttribute("id", "user_devices_activity")

        let title_elem = document.createElement('h2');
        title_elem.innerText = 'Tracked Devices';
        div_outer.appendChild(title_elem);
        main_center.appendChild(div_outer)

        let div_date = document.createElement("div")
        div_date.innerHTML = "<p>Number of tracked devices: " + (devices.get("OS").length) + "</p>";

        let device_list = document.createElement("table");
        device_list.style.margin = 'auto';
        device_list.innerHTML += "<tr><th>Device Model</th><th>Device OS</th><th>Last known country</th></tr>";
        for(let i = 0; i < devices.get("OS").length; i++) {
            const device = devices.get("Device Model")[i];
            const OS = devices.get("OS")[i];
            const country = devices.get("Device Last Country")[i];
            device_list.innerHTML += "<tr><td>" + device + "</td><td>" + OS + "</td><td>" + country + "</td></tr>";
        }
        div_outer.appendChild(div_date);
        div_outer.appendChild(device_list);
        
    } catch (error) {
        
        no_data_found('user_devices_activity', 'Tracked Devices', 'There were no tracked devices found in your data.');
        console.log(error)
    }
}

async function user_activity(zipfs) {
    try {
        let file = zipfs.find("Takeout/Access Log Activity/Activities - A list of Google services accessed by.csv"); // find the responsible file for the analysis
        let fileData = await file.getText(); // gets the text stored in the file.
        let activity = parse_csv(fileData); // parse the csv text file to a map.

        let products = activity.get("Product Name"); // retrieve all data from the product name column

        if (products.length !== 0) {
            // add the found information to the webpage
            let services_history = document.getElementById('user_devices_activity');

            if(typeof services_history === 'undefined') {
                let main_elem = document.getElementById('center')
                services_history = document.createElement("div");
                services_history.classList.add("statistic_block")
                services_history.setAttribute("id", "google_play")
                main_elem.appendChild(services_history)
            }

            let div_inner_stats = document.createElement("div")
            div_inner_stats.setAttribute("id", "services_history")
            services_history.appendChild(div_inner_stats);
            let div_inner_map = document.createElement("div")
            services_history.appendChild(div_inner_map)
            let inner_map = document.createElement("canvas")
            inner_map.setAttribute("id", "servicesHistoryChart")
            div_inner_map.appendChild(inner_map)

            div_inner_stats.innerHTML = "<p><h2>Service Activity</h2>Number of activities found: " + (products.length) + "</p>";

            let product_count = new Map(); // create a new map to count all appearances
            for (let i = 0; i < products.length; i++) {
                let value = products[i];
                if (product_count.has(value)) {
                    product_count.get(value).count++;
                } else {
                    product_count.set(value, {count: 1});
                }
            }
            // create label and value arrays for the chart
            let label_array = Array.from(product_count.keys());
            let value_array = []
            for (let i = 0; i < label_array.length; i++) {
                value_array.push(product_count.get(label_array[i]).count);
            }

            // create the chart object
            const data = {
                labels: label_array,
                datasets: [{
                    label: "use of services tracked",
                    backgroundColor: '#11999e',
                    borderColor: '#11999e',
                    data: value_array,
                }]
            };
            const config = {
                type: 'bar',
                data: data,
                options: {
                    scales: {
                        y: {
                            beginAtZero: true,
                            type: 'logarithmic'
                        }
                    }
                },
            };
            let myChart = new Chart(
                document.getElementById('servicesHistoryChart'),
                config
            );
            
        } else {
            
            no_data_found('services_history', 'Service Activity', 'There was no service activity found in your data. I.E. Connections made to Google Services by your devices.');
        }
    } catch (error) {
        
        no_data_found('services_history', 'Service Activity', 'There was no service activity found in your data. I.E. Connections made to Google Services by your devices.');
    }
}

async function google_play_store_reviews(zipfs) {
    try {
        let file = zipfs.find("Takeout/Google Play Store/Reviews.json") // find the responsible file for the analysis
        let fileData = await file.getText(); // gets the text stored in the file

        // get the review history data
        let review_history = JSON.parse(fileData);

        // Object for storing all stars
        let review_stars = {};

        for (let i = 1; i < 6; i++) {
          review_stars[i] = 0
        }

        for (let i = 0; i < review_history.length; i++) {
            let review = review_history[i]["review"]["starRating"];
            review_stars[review] += 1;
        }

        if (review_history.length !== 0) {

            // display total number of reviews

            let google_play_element = document.getElementById('google_play');

            if(typeof google_play_element === 'undefined') {
                let main_elem = document.getElementById('center')
                google_play_element = document.createElement("div");
                google_play_element.classList.add("statistic_block")
                google_play_element.setAttribute("id", "google_play")
                main_elem.appendChild(google_play_element)
            }

            let div_inner_stats = document.createElement("div")
            div_inner_stats.setAttribute("id", "google_play_store_reviews")
            google_play_element.appendChild(div_inner_stats);
            let div_inner_map = document.createElement("div")
            google_play_element.appendChild(div_inner_map)
            let inner_map = document.createElement("canvas")
            inner_map.setAttribute("id", "google_reviews_chart")
            div_inner_map.appendChild(inner_map)

            div_inner_stats.innerHTML = "<p><h2>Play Store Reviews</h2>In total you reviewed " + review_history.length + " apps in Google Play </p>"

            let labels
            let graph_header
            let data_graph = [];

            graph_header = "The amount of reviews tracked by Google"
            labels = ["1 star", "2 stars", "3 stars", "4 stars", "5 stars"]

            data_graph = Object.values(review_stars);

            const data = {
                labels: labels,
                datasets: [{
                    label: graph_header,
                    backgroundColor: [
                        "#2ecc71",
                        "#3498db",
                        "#95a5a6",
                        "#9b59b6",
                        "#f1c40f",
                    ],
                    data: data_graph,
                }]
            };
            const config = {
                type: 'pie',
                data: data,
            };
            let myChart = new Chart(
                document.getElementById('google_reviews_chart'),
                config
            );
            
        } else {
            no_data_found('google_play_store_reviews', 'Play Store Reviews', 'There were no play store reviews found in your data.');
            
        }
    } catch (error) {
        no_data_found('google_play_store_reviews', 'Play Store Reviews', 'There were no play store reviews found in your data.');
        
        console.log(error)
    }
}


function parse_csv (text) {
    let lines = text.split(/\r\n|\n/);
    let headers = lines[0].split(','); // store all the column headers in an array
    let columns = new Map(); // create a map to store the values of each column
    for (let i = 0; i < headers.length; i++) { // create a key and array value for each header
        columns.set(headers[i], []);
    }
    for (let i=1; i < lines.length; i++) { // fill the columns with values
        lines[i] = lines[i].replace(/"[^"]+"/g, function(v) { // change a comma within qoutes to ;
            return v.replace(/,/g, ';');
        });
        let values = lines[i].split(','); // split the csv on its commas
        for (let j = 0; j < headers.length; j++) {
            if (values.length === headers.length){
                columns.get(headers[j]).push(values[j]); // store the values
            }
        }
    }
    return columns;
}

/** @deprecated */
async function find_picture_json(zipfs) {
    try {
        let path = "Takeout/Google Photos"; // becomes path to pictures
        let paths = []; // in here all paths that are found will be stored.
        await find_json_rec(zipfs, path, paths);
        let people_found = {};

        let people_were_found = false;

        for (let i = 0; i < paths.length; i++) {
            let file = zipfs.find(paths[i]);
            let fileData = await file.getText(); // gets the text stored in the file.
            let picture_metadata = JSON.parse(fileData);
            let people = picture_metadata["people"]

            if (typeof people !== 'undefined') {
                people_were_found = true;
                for (let j = 0; j < people.length; j++) {
                    let key = people[j]["name"]
                    if (key in people_found) {
                        people_found[key]++;
                    } else {
                        people_found[key] = 1;
                    }
                }
            }
        }

        if (people_were_found) {

            let people_found_element = document.getElementById('center');

            let div_outer = document.createElement("div");
            div_outer.classList.add("statistic_block");
            let title_elem = document.createElement('h2');
            title_elem.innerText = 'People in Google Photos';
            div_outer.appendChild(title_elem);
            people_found_element.appendChild(div_outer);

            let div_inner_map = document.createElement("div")
            div_outer.appendChild(div_inner_map)
            let inner_map = document.createElement("canvas")
            inner_map.setAttribute("id", "PeopleFoundChart")
            div_inner_map.appendChild(inner_map)

            let labels;
            // the label to display on top of the graph
            // the data to fill the graph with
            let data_graph = [];
            // data within on year
            labels = [];
            let keyset = Object.keys(people_found);
            for (let i = 0; i < keyset.length; i++) {
                labels.push(keyset[i]);
                data_graph.push(people_found[keyset[i]])
            }

            // plot the data as a bar chart
            const data = {
                labels: labels,
                datasets: [{
                    label: "people recognizes in data",
                    backgroundColor: '#11999e',
                    borderColor: '#11999e',
                    data: data_graph,
                }]
            };
            const config = {
                type: 'bar',
                data: data,
                options: {
                    scales: {
                        y: {
                            beginAtZero: true
                        }
                    }
                },
            };
            let myChart = new Chart(
                document.getElementById('PeopleFoundChart'),
                config
            );
            
        } else {
            no_data_found('google_play_store_reviews', 'People in Google Photos', 'There were no people found in your Google Photos data.');
            
        }
    } catch (error) {
        
        no_data_found('google_play_store_reviews', 'People in Google Photos', 'There were no people found in your Google Photos data.');
    }
}

/** @deprecated */
async function find_json_rec(zipfs, path,  paths) { // will recursively find every json file within the given path
    let files_in_folder = zipfs.find(path);
    let children = await files_in_folder.children;

    for (let i = 0; i < children.length; i++) { //loops over all children of the folder
        let name = children[i].getFullname();
        if (children[i].name.includes(".")) { // checks if it is a folder or a file
            if (name.includes(".json")) { // checks if it is a json file
                paths.push(name)
            }
        } else {
            await find_json_rec(zipfs, name, paths) // it is a folder so we loop again
        }
    }
}

async function create_location_map(zipfs) {
    try {
        let file = zipfs.find("Takeout/Location History/Location History.json"); // find the responsible file for the analysis
        let fileData = await file.getText(); // gets the text stored in the file.

        const custom_pin = L.icon({
            iconUrl: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAABSCAMAAAAhFXfZAAAC91BMVEVMaXEzeak2f7I4g7g3g7cua5gzeKg8hJo3grY4g7c3grU0gLI2frE0daAubJc2gbQwd6QzeKk2gLMtd5sxdKIua5g1frA2f7IydaM0e6w2fq41fK01eqo3grgubJgta5cxdKI1f7AydaQydaMxc6EubJgvbJkwcZ4ubZkwcJwubZgubJcydqUydKIxapgubJctbJcubZcubJcvbJYubJcvbZkubJctbJctbZcubJg2f7AubJcrbZcubJcubJcua5g3grY0fq8ubJcubJdEkdEwhsw6i88vhswuhcsuhMtBjMgthMsrg8srgss6is8qgcs8i9A9iMYtg8spgcoogMo7hcMngMonf8olfso4gr8kfck5iM8jfMk4iM8he8k1fro7itAgesk2hs8eecgzfLcofssdeMg0hc4cd8g2hcsxeLQbdsgZdcgxeLImfcszhM0vda4xgckzhM4xg84wf8Yxgs4udKsvfcQucqhUndROmdM1fK0wcZ8vb5w0eqpQm9MzeKhXoNVcpdYydKNWn9VZotVKltJFjsIwcJ1Rms9OlslLmtH///8+kc9epdYzd6dbo9VHkMM2f7FHmNBClM8ydqVcpNY9hro3gLM9hLczealQmcw3fa46f7A8gLMxc6I3eagyc6FIldJMl9JSnNRSntNNl9JPnNJFi75UnM9ZodVKksg8kM45jc09e6ZHltFBk883gbRBh7pDk9EwcaBzn784g7dKkcY2i81Om9M7j85Llc81is09g7Q4grY/j9A0eqxKmdFFltBEjcXf6fFImdBCiLxJl9FGlNFBi78yiMxVndEvbpo6js74+vx+psPP3+o/ks5HkcpGmNCjwdZCkNDM3ehYoNJEls+lxNkxh8xHks0+jdC1zd5Lg6r+/v/H2ufz9/o3jM3t8/edvdM/k89Th61OiLBSjbZklbaTt9BfptdjmL1AicBHj8hGk9FAgK1dkLNTjLRekrdClc/k7fM0icy0y9tgp9c4jc2NtM9Dlc8zicxeXZn3AAAAQ3RSTlMAHDdTb4yPA+LtnEQmC4L2EmHqB7XA0d0sr478x4/Yd5i1zOfyPkf1sLVq4Nh3FvjxopQ2/STNuFzUwFIwxKaejILpIBEV9wAABhVJREFUeF6s1NdyFEcYBeBeoQIhRAkLlRDGrhIgY3BJL8CVeKzuyXFzzjkn5ZxzzuScg3PO8cKzu70JkO0LfxdTU//pM9vTu7Xgf6KqOVTb9X7toRrVEfBf1HTVjZccrT/2by1VV928Yty9ZbVuucdz90frG8DBjl9pVApbOstvmMuvVgaNXSfAAd6pGxpy6yxf5ph43pS/4f3uoaGm2rdu72S9xzOvMymkZFq/ptDrk90mhW7e4zl7HLzhxGWPR20xmSxJ/VqldG5m9XhaVOA1DadsNh3Pu5L2N6QtPO/32JpqQBVVk20oy/Pi2s23WEvyfHbe1thadVQttvm7Llf65gGmXK67XtupyoM7HQhmXdLS8oGWJNeOJ3C5fG5XCEJnkez3/oFdsvgJ4l2ANZwhrJKk/7OSXa+3Vw2WJMlKnGkobouYk6T0TyX30klOUnTD9HJ5qpckL3EW/w4XF3Xd0FGywXUrstrclVsqz5Pd/sXFYyDnPdrLcQODmGOK47IZb4CmibmMn+MYRzFZ5jg33ZL/EJrWcszHmANy3ARBK/IXtciJy8VsitPSdE3uuHxzougojcUdr8/32atnz/ev3f/K5wtpxUTpcaI45zusVDpYtZi+jg0oU9b3x74h7+n9ABvYEZeKaVq0sh0AtLKsFtqNBdeT0MrSzwwlq9+x6xAO4tgOtSzbCjrNQQiNvQUbUEubvzBUeGw26yDCsRHCoLkTHDa7IdOLIThs/gHvChszh2CimE8peRs47cxANI0lYNB5y1DljpOF0IhzBDPOZnDOqYYbeGKECbPzWnXludPphw5c2YBq5zlwXphIbO4VDCZ0gnPfUO1TwZoYwAs2ExPCedAu9DAjfQUjzITQb3jNj0KG2Sgt6BHaQUdYzWz+XmBktOHwanXjaSTcwwziBcuMOtwBmqPrTOxFQR/DRKKPqyur0aiW6cULYsx6tBm0jXpR/AUWR6HRq9WVW6MRhIq5jLyjbaCTDCijyYJNpCajdyobP/eTw0iexBAKkJ3gA5KcQb2zBXsIBckn+xVv8jkZSaEFHE+jFEleAEfayRU0MouNoBmB/L50Ai/HSLIHxcrpCvnhSQAuakKp2C/YbCylJjXRVy/z3+Kv/RrNcCo+WUzlVEhzKffnTQnxeN9fWF88fiNCUdSTsaufaChKWInHeysygfpIqagoakW+vV20J8uyl6TyNKEZWV4oRSPyCkWpgOLSbkCObT8o2r6tlG58HQquf6O0v50tB7JM7F4EORd2dx/K0w/KHsVkLPaoYrwgP/y7krr3SSMA4zj+OBgmjYkxcdIJQyQRKgg2viX9Hddi9UBb29LrKR7CVVEEEXWojUkXNyfTNDE14W9gbHJNuhjDettN3ZvbOvdOqCD3Jp/9l+/wJE+9PkYGjx/fqkys3S2rMozM/o2106rfMUINo6hVqz+eu/hd1c4xTg0TAfy5kV+4UG6+IthHTU9woWmxuKNbTfuCSfovBCxq7EtHqvYL4Sm6F8GVxsSXHMQ07TOi1DKtZxjWaaIyi4CXWjxPccUw8WVbMYY5wxC1mzEyXMJWkllpRloi+Kkoq69sxBTlElF6aAxYUbjXNlhlDZilDnM4U5SlN5biRsRHnbx3mbeWjEh4mEyiuJDl5XcWVmX5GvNkFgLWZM5qwsop4/AWfLhU1cR7k1VVvcYCWRkOI6Xy5gmnphCYIkvzuNYzHzosq2oNk2RtSs8khfUOfHIDgR6ysYBaMpl4uEgk2U/oJTs9AaTSwma7dT69geAE2ZpEjUsn2ieJNHeKfrI3EcAGJ2ZaNgVuC8EBctCLc57P5u5led6IOBkIYkuQMrmmjChs4VkfOerHqSBkPzZlhe06RslZ3zMjk2sscqKwY0RcjKK+LWbzd7KiHhkncs/siFJ+V5eXxD34B8nVuJEpGJNmxN2gH3vSvp7J70tF+D1Ej8qUJD1TkErAND2GZwTFg/LubvmgiBG3SOvdlsqFQrkEzJCL1rstlnVFROixZoDDSuXQFHESwVGlcuQcMb/b42NgjLowh5MTDFE3vNB5qStRIErdCQEh6pLPR92anSUb/wAIhldAaDMpGgAAAABJRU5ErkJggg==',
            shadowUrl: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACkAAAApCAQAAAACach9AAACMUlEQVR4Ae3ShY7jQBAE0Aoz/f9/HTMzhg1zrdKUrJbdx+Kd2nD8VNudfsL/Th///dyQN2TH6f3y/BGpC379rV+S+qqetBOxImNQXL8JCAr2V4iMQXHGNJxeCfZXhSRBcQMfvkOWUdtfzlLgAENmZDcmo2TVmt8OSM2eXxBp3DjHSMFutqS7SbmemzBiR+xpKCNUIRkdkkYxhAkyGoBvyQFEJEefwSmmvBfJuJ6aKqKWnAkvGZOaZXTUgFqYULWNSHUckZuR1HIIimUExutRxwzOLROIG4vKmCKQt364mIlhSyzAf1m9lHZHJZrlAOMMztRRiKimp/rpdJDc9Awry5xTZCte7FHtuS8wJgeYGrex28xNTd086Dik7vUMscQOa8y4DoGtCCSkAKlNwpgNtphjrC6MIHUkR6YWxxs6Sc5xqn222mmCRFzIt8lEdKx+ikCtg91qS2WpwVfBelJCiQJwvzixfI9cxZQWgiSJelKnwBElKYtDOb2MFbhmUigbReQBV0Cg4+qMXSxXSyGUn4UbF8l+7qdSGnTC0XLCmahIgUHLhLOhpVCtw4CzYXvLQWQbJNmxoCsOKAxSgBJno75avolkRw8iIAFcsdc02e9iyCd8tHwmeSSoKTowIgvscSGZUOA7PuCN5b2BX9mQM7S0wYhMNU74zgsPBj3HU7wguAfnxxjFQGBE6pwN+GjME9zHY7zGp8wVxMShYX9NXvEWD3HbwJf4giO4CFIQxXScH1/TM+04kkBiAAAAAElFTkSuQmCC',

            iconSize: [25, 41],
            shadowSize: [41, 41],
            iconAnchor: [12, 41],
            popupAnchor: [1, -34],
            tooltipAnchor: [16, -28]
        });


        let location_history = JSON.parse(fileData);
        // get the field of browser history (because of the structure of the json file)
        location_history = await location_history["locations"];

        if (location_history.length !==0) {

            let location_element = document.getElementById('center');

            let div_outer = document.createElement("div");
            div_outer.classList.add("statistic_block");
            let title_elem = document.createElement('h2');
            title_elem.innerText = 'Locations';
            let paragraph = document.createElement('p');
            paragraph.innerHTML = 'In total Google has stored ' + location_history.length + ' locations in your data.';
            paragraph.innerHTML += '<br>Note: Each datapoint on the map is an aggregation of multiple points. The actual information is accurate down to meters.';
            div_outer.appendChild(title_elem);
            div_outer.appendChild(paragraph);
            location_element.appendChild(div_outer);

            let div_inner_map = document.createElement("div")
            div_outer.appendChild(div_inner_map)
            div_inner_map.setAttribute("id", "location_map")
            div_inner_map.style.width = "auto"
            div_inner_map.style.height = "50vh"

            let inner_map = document.createElement("canvas")
            inner_map.setAttribute("id", "PeopleFoundChart")
            div_inner_map.appendChild(inner_map)

            let location_map = L.map('location_map').setView([51.505, -0.09], 3);


            L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw', {
                maxZoom: 18,
                attribution: '',
                id: 'mapbox/streets-v11',
                tileSize: 512,
                zoomOffset: -1
            }).addTo(location_map);

            let map_markers = {}

            for (let i = 0; i < location_history.length; i++) {
                let latitude = location_history[i].latitudeE7;
                let longitude = location_history[i].longitudeE7;

                latitude = latitude / 10000000
                longitude = longitude / 10000000

                let latitude_short = (Math.round(latitude * 4) / 4).toFixed(2);
                let longitude_short = (Math.round(longitude * 4) / 4).toFixed(2);

                let lat_long = latitude_short + "$" + longitude_short;

                if (lat_long in map_markers) {
                    map_markers[lat_long]++;
                } else {
                    map_markers[lat_long] = 1;
                }
            }

            for (const [key, value] of Object.entries(map_markers)) {
                let coordinates = key.split("$")
                let converted_latitude = parseFloat(coordinates[0]);
                let converted_longitude = parseFloat(coordinates[1]);

                L.marker([converted_latitude, converted_longitude], {icon: custom_pin}).addTo(location_map)
                    .bindPopup("<b>locations found</b><br />" + value);
            }
            
        } else {
            
            no_data_found('location_map', 'Locations', 'There were no locations found in your data.');
        }
    } catch (error) {
        
        no_data_found('location_map', 'Locations', 'There were no locations found in your data.');
    }
}
