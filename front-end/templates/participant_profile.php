<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Participant</title>
    <link rel="stylesheet" href="/css/style.css">
</head>
<body>
    <header>Survey Google data collection</header>
    <div class="center">
        <h2>Personal Page</h2>
        <div id="logout">
            <h3>Logout</h3>
            <a href="/logout">
                <button>Logout</button>
            </a>
        </div>
        <div id="data_dump">
            <h3>Request data that is stored in the database</h3>
            <p>
                As an participant in our research you have all the right to know which data is collected by us on you.
                The data that we have gather can be requested by clicking on the button underneath. This data will
                consist out of a web page which shows all the answers and data points that you have provided.
            </p>
            <a target="_blank" href="/participant/collected_data">
                <button>Request Data</button>
            </a>
        </div>
        <div id="analysis">
            <h3>Rerun analysis</h3>
            <p>
                If you would like to see the results again of the analysis, you can rerun it on the page which is linked
                to by this button.
            </p>
            <a href="/rerun_analysis">
                <button>Re-run analysis on .zip</button>
            </a>
        </div>
        <div id="limit_data">
            <h3>How to limit data collection?</h3>
            <p>
                Our goal of researchers is not only to make you aware of the data collection but also to teach you on
                how to limit or completely prevent collection in the first place.
            </p>
            <a href="/front-end/pdf/guide.pdf" target="_blank">
                <button>Open Guide</button>
            </a>
        </div>
        <div id="revoke">
            <h3>Revoke all your data</h3>
            <p>
                If you are sure that you do not longer want to contribute to this research, you can revoke all data that
                has been stored in our database. <b>BE AWARE!</b> This operation is irreversible!
            </p>
            <button class="red" id="revoke_button">Revoke Data</button>
        </div>
    </div>
    <footer>
        <div class="uid">My ID: <?php echo $participant->getUIDHash(); ?></div>
    </footer>
    <div class="popup" id="revoke_popup">
        <div class="popup_container">
            <div class="popup_title">Are you sure?</div>
            <div class="popup_body">
                <div>
                    This action will remove all the data that is stored on our server about you. <br>This process
                    cannot be undone. If you just want to pause the questionnaire, you can close this window.
                </div>
                <div>
                    <button id="return_button">Go back</button>
                    <button id="continue_button" class="red" data-link="/participant/revoke">Remove all data</button>
                </div>
            </div>
        </div>
    </div>
    <script src="/js/revoke_popup.js"></script>
</body>
</html>