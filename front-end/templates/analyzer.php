<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Analysis</title>
    <link rel="stylesheet" href="/css/style.css">
    <link rel="stylesheet" href="/css/leaflet.css">
</head>
<body>
<header>Survey Google data collection</header>
<div class="center" id="main_elem" style="text-align: center">
    <div id="center">
        <h2>Analysis</h2>
        <p>
            This page will analyze the data that Google has collected about you. In order to generate the statistics you
            will be asked to download your data from Google and submit it to this page using the file upload button beneath.
            Your data will remain on your computer, the entire analysis happens locally and no data will be sent to our
            server.
        </p>
        <p>
            Please open the instructions for generating the Takeout file
            <a href="/analysis/instructions" target="_blank" title="Takeout Instructions">here</a>. (Will open in a new tab).
            Please read these carefully in order to make sure that you will get the best results out of the analysis. Be aware
            that the creation of the zip-file may take some time. Feel free to close this window and come back to the website
            when it is ready.
        </p>
        <div class="input_block">
            <input type=file id=input onload=init()>
        </div>
    </div>
    <div id="loader">
        <div class="loading circle">
            <div class="element e1"></div>
            <div class="element e2"></div>
            <div class="element e3"></div>
            <div class="element e4"></div>
        </div>
    </div>
    <div id="finished">
        <p>
            You have reached the end of the analysis tool. On the next page there will be the last couple of questions.
            It is advised to export the analysis that has just been created, since you might want to use them while you are
            answering these.
        </p>
        <div>
            <button onclick="export_analysis()">Export Analysis</button>
            <a href="/post-analysis">
                <button>Continue</button>
            </a>
        </div>
    </div>
</div>

<footer>
    <div class="uid">My ID: <?php echo $participant->getUIDHash(); ?></div>
    <div class="pagecounter">
        4/6 <div class="progressbar_background"><div class="progressbar" style="width: 67%"></div></div>
    </div>
    <div class="revoke">
        <button class="red" id="revoke_button">Remove all data</button>
    </div>
</footer>
<div class="popup" id="revoke_popup">
    <div class="popup_container">
        <div class="popup_title">Are you sure?</div>
        <div class="popup_body">
            <div>
                This action will remove all the data that is stored on our server about you. <br>This process
                cannot be undone. If you just want to pause the questionnaire, you can close this window.
            </div>
            <div>
                <button id="return_button">Go back</button>
                <button id="continue_button" class="red" data-link="/participant/revoke">Remove all data</button>
            </div>
        </div>
    </div>
</div>
<script src="/js/chart.min.js"></script>
<script src="/js/zip-fs.min.js"></script>
<script src="/js/zipreader.js"></script>
<script src="/js/leaflet.js"></script>
<script src="/js/FileSaver.min.js"></script>
<script src="/js/dom-to-image.min.js"></script>
<script src="/js/export_analysis.js"></script>
<script src="/js/revoke_popup.js"></script>
</body>
</html>