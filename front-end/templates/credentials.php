<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Credentials</title>
    <link rel="stylesheet" href="/css/style.css">
</head>
<body>
    <header>Survey Google data collection</header>
    <div class="center">
        <h2>Credentials</h2>
        <p>Over here you can find your user ID and password. This information will be required in order to login after
        the survey has been completed in order to download for example the collected data.<br>
        Make sure to store this information on a secure location! This information is only accessible once and will not
        be provided again.</p>
        <p>
            Your user ID: <span class="credentials"><?php echo $hash ?></span><br><br>
            Your password: <span class="credentials"><?php echo $password ?></span> Be aware that the password is case-sensitive!
        </p>
        <label>
            <input type="checkbox" id="check">
            I saved my credentials on a safe location, and I understand that I will not be able to request them ever again.
        </label>
        <div class="button">
            <a href="#">
                <button>Continue</button>
            </a>
        </div>
    </div>
    <footer>
        <div class="pagecounter">
            2/6
            <div class="progressbar_background">
                <div class="progressbar" style="width: 33%"></div>
            </div>
        </div>
    </footer>
    <script src="js/credentials_check.js"></script>
</body>
</html>