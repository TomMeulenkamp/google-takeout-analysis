<!DOCTYPE html>
<html lang="en">
<head>
    <title>Your Data</title>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="/css/style.css">
</head>
<body>
<header>Your Data</header>
<div class="center">
    <h2>Your Collected Data</h2>
    <p>On this page we will present all the data that we have stored on you.</p>

    <h2>Entered Questions</h2>
    <table>
        <tr>
            <th>Question</th>
            <th>Answer</th>
        </tr>
        <?php
            foreach($participant->getAnswers() as $qid => $answer) {
                echo "<tr><td>{$answer['question']}</td><td>{$answer['value']}</td></tr>";
            }
        ?>
    </table>
</div>
<footer>
</footer>
</body>
</html>