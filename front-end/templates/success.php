<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Success</title>
        <meta charset="UTF-8">
        <link rel="stylesheet" href="/css/style.css">
    </head>
    <body>
    <header>Success</header>
    <div class="center">
        <h1 class= "error"><?php echo $message ?></h1>
    </div>
    <footer>
    </footer>
    </body>
</html>