<tr>
    <td class="likert_question"><?php echo $this->getQuestion() ?></td>
    <?php
        $i = 1;
        foreach($this->getAnswerOptions() as $answer) {
            echo "<td class=\"likert_answer\"><input type=\"radio\" id=\"{$this->getId()}{$i}\" name='{$this->getId()}' value=\"{$answer->getId()}\" required></td>";
            $i += 1;
        }
    ?>
</tr>
