<?php
    require_once dirname(__DIR__, 2) . '/back-end/models/Question.php';
    require_once dirname(__DIR__, 2) . '/back-end/models/LikertQuestion.php';
    require_once dirname(__DIR__, 2) . '/back-end/models/OpenQuestion.php';
?>

<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>Post-Analysis</title>
        <link rel="stylesheet" href="/css/style.css">
    </head>
    <body>
        <header>
            Survey Google data collection
        </header>
        <form method="post" action="/post-analysis/submit" class="center">
            <h2>General</h2>
            <?php
                Question::get(73)->getHTML();
                Question::get(74)->getHTML();
                OpenQuestion::get(75)->getHTML();
                OpenQuestion::get(76)->getHTML();
                OpenQuestion::get(77)->getHTML();
                OpenQuestion::get(78)->getHTML();
                Question::get(79)->getHTML();
                OpenQuestion::get(80)->getHTML();
                Question::get(81)->getHTML();
                OpenQuestion::get(82)->getHTML();
                Question::get(83)->getHTML();
                OpenQuestion::get(85)->getHTML();
            ?>
            <h2>Google Chrome</h2>
            <table class="likert_table">
                <?php require dirname(__DIR__, 2) . '/front-end/html/likert_question.html'; ?>
                <?php
                for($i = 47; $i < 52; $i++) {
                    LikertQuestion::get($i)->getHTML();
                }
                LikertQuestion::get(88)->getHTML();
                ?>
            </table>
            <h2>Google Playstore</h2>
            <table class="likert_table">
                <?php require dirname(__DIR__, 2) . '/front-end/html/likert_question.html'; ?>
                <?php
                for($i = 52; $i < 58; $i++) {
                    LikertQuestion::get($i)->getHTML();
                }
                ?>
            </table>
            <h2>YouTube and YouTube Music</h2>
            <table class="likert_table">
                <?php require dirname(__DIR__, 2) . '/front-end/html/likert_question.html'; ?>
                <?php
                for($i = 58; $i < 64; $i++) {
                    LikertQuestion::get($i)->getHTML();
                }
                ?>
            </table>
            <h2>Google Maps</h2>
            <table class="likert_table">
                <?php require dirname(__DIR__, 2) . '/front-end/html/likert_question.html'; ?>
                <?php
                for($i = 64; $i < 69; $i++) {
                    LikertQuestion::get($i)->getHTML();
                }
                LikertQuestion::get(89)->getHTML();
                ?>
            </table>
            <div class="button">
                <input type="submit" value="Submit">
            </div>
        </form>
        <footer>
            <div class="uid">My ID: <?php echo $participant->getUIDHash(); ?></div>
            <div class="pagecounter">
                5/6
                <div class="progressbar_background">
                    <div class="progressbar" style="width: 83%"></div>
                </div>
            </div>
            <div class="revoke">
                <button class="red" id="revoke_button">Remove all data</button>
            </div>
        </footer>
        <div class="popup" id="revoke_popup">
            <div class="popup_container">
                <div class="popup_title">Are you sure?</div>
                <div class="popup_body">
                    <div>
                        This action will remove all the data that is stored on our server about you. <br>This process
                        cannot be undone. If you just want to pause the questionnaire, you can close this window.
                    </div>
                    <div>
                        <button id="return_button">Go back</button>
                        <button id="continue_button" class="red" data-link="/participant/revoke">Remove all data</button>
                    </div>
                </div>
            </div>
        </div>
    </body>
    <script src="/js/revoke_popup.js"></script>
</html>