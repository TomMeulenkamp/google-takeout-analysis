<?php
    require_once dirname(__DIR__, 2) . '/back-end/models/Question.php';
    require_once dirname(__DIR__, 2) . '/back-end/models/LikertQuestion.php';
?>

<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>Pre-Analysis</title>
        <link rel="stylesheet" href="/css/style.css">
    </head>
    <body>
        <header>
            Survey Google data collection
        </header>
        <form method="post" action="/pre-analysis/submit" class="center">
            <h2>General</h2>
            <?php for($i = 70; $i < 73; $i++) {
                Question::get($i)->getHTML();
            } ?>
            <p>For the following questions, please state your <b>expectations</b>.</p>
            <table class="likert_table">
                <?php require dirname(__DIR__, 2) . '/front-end/html/likert_question.html'; ?>
                <?php
                for($i = 1; $i < 8; $i++) {
                    if($i == 3) {
                        continue;
                    }
                    LikertQuestion::get($i)->getHTML();
                }
                ?>
            </table>
            <h2>Personal Information (Name, Address, etc.)</h2>
            <table class="likert_table">
                <?php require dirname(__DIR__, 2) . '/front-end/html/likert_question.html'; ?>
                <?php
                for($i = 8; $i < 14; $i++) {
                    LikertQuestion::get($i)->getHTML();
                }
                ?>
            </table>
            <h2>Location</h2>
            <table class="likert_table">
                <?php require dirname(__DIR__, 2) . '/front-end/html/likert_question.html'; ?>
                <?php
                for($i = 14; $i < 20; $i++) {
                    LikertQuestion::get($i)->getHTML();
                }
                ?>
            </table>
            <h2>Browsing History</h2>
            <table class="likert_table">
                <?php require dirname(__DIR__, 2) . '/front-end/html/likert_question.html'; ?>
                <?php
                for($i = 20; $i < 24; $i++) {
                    LikertQuestion::get($i)->getHTML();
                }
                ?>
            </table>
            <p>Please rate your <strong>expectations</strong> for the following statements.</p>
            <h2>Google Chrome</h2>
            <table class="likert_table">
                <?php require dirname(__DIR__, 2) . '/front-end/html/likert_question.html'; ?>
                <?php
                for($i = 24; $i < 29; $i++) {
                    LikertQuestion::get($i)->getHTML();
                }
                LikertQuestion::get(86)->getHTML();
                ?>
            </table>
            <h2>Google Playstore</h2>
            <table class="likert_table">
                <?php require dirname(__DIR__, 2) . '/front-end/html/likert_question.html'; ?>
                <?php
                for($i = 29; $i < 35; $i++) {
                    LikertQuestion::get($i)->getHTML();
                }
                ?>
            </table>
            <h2>YouTube and YouTube Music</h2>
            <table class="likert_table">
                <?php require dirname(__DIR__, 2) . '/front-end/html/likert_question.html'; ?>
                <?php
                for($i = 35; $i < 41; $i++) {
                    LikertQuestion::get($i)->getHTML();
                }
                ?>
            </table>
            <h2>Google Maps</h2>
            <table class="likert_table">
                <?php require dirname(__DIR__, 2) . '/front-end/html/likert_question.html'; ?>
                <?php
                for($i = 41; $i < 46; $i++) {
                    LikertQuestion::get($i)->getHTML();
                }
                LikertQuestion::get(87)->getHTML();
                ?>
            </table>
            <div class="button">
                <input type="submit" value="Submit">
            </div>
        </form>
        <footer>
            <div class="uid">My ID: <?php echo $participant->getUIDHash(); ?></div>
            <div class="pagecounter">
                3/6
                <div class="progressbar_background">
                    <div class="progressbar" style="width: 50%"></div>
                </div>
            </div>
            <div class="revoke">
                <button class="red" id="revoke_button">Remove all data</button>
            </div>
        </footer>
        <div class="popup" id="revoke_popup">
            <div class="popup_container">
                <div class="popup_title">Are you sure?</div>
                <div class="popup_body">
                    <div>
                        This action will remove all the data that is stored on our server about you. <br>This process
                        cannot be undone. If you just want to pause the questionnaire, you can close this window.
                    </div>
                    <div>
                        <button id="return_button">Go back</button>
                        <button id="continue_button" class="red" data-link="/participant/revoke">Remove all data</button>
                    </div>
                </div>
            </div>
        </div>
    </body>
    <script src="/js/revoke_popup.js"></script>
</html>