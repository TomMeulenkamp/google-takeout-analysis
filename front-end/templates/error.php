<!DOCTYPE html>
<html lang="en">
    <head>
        <title><?php echo $title ?></title>
        <meta charset="UTF-8">
        <link rel="stylesheet" href="/css/style.css">
    </head>
    <body>
    <header><?php echo $head ?></header>
    <div class="center">
        <h1 class= "error"><?php echo $message ?></h1>
    </div>
    <footer>
    </footer>
    </body>
</html>