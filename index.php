<?php
/**
 * A simple PHP router.
 * Source: https://github.com/steampixel/simplePHPRouter/tree/master
 */

require_once 'back-end/Route.php';
require_once 'back-end/models/Participant.php';
require_once 'back-end/models/Question.php';
require_once 'back-end/StatusCodes.php';

$path = parse_url($_SERVER['REQUEST_URI']);
$cookie_name = "tommeulenkamp_uid";

// HELPER FUNCTIONS
/**
 * Redirects the participant to the proper section on the website.
 * @param Participant $participant
 */
function progressToPage(Participant $participant):void {
    switch (Participant::$PROGRESS_OPTIONS[$participant->getProgress()]) {
        case "PRE_ANALYSIS":
            header("Location: /pre-analysis");
            break;
        case "ANALYSIS":
            header("Location: /analysis");
            break;
        case "POST_ANALYSIS":
            header("Location: /post-analysis");
            break;
        case "FINISHED":
            header("Location: /participant");
            break;
    }
}

// ROUTES

// STATIC ROUTES
Route::add('/css/(.*)', function ($file) {
    header("Content-Type: text/css");
    require_once __DIR__ . '/front-end/css/' . $file;
});

Route::add('/js/(.*)', function ($file) {
    if(str_contains($file, '.map')) {
        StatusCodes::HTTPStatus(404);
        return;
    }
    header("Content-Type: text/javascript");
    require_once __DIR__ . '/front-end/js/' . $file;
});

Route::add('/img/(.*)', function ($file) {
    header("Content-Type: image/png");
    require_once __DIR__ . '/front-end/img/' . $file;
});

// PARTICIPANT MANAGEMENT
Route::add('/login', function () use ($cookie_name) {
    /*
     * 1a. Check if the participant is already signed in.
     * 1b. Display login page.
     */
    if(isset($_COOKIE[$cookie_name])) {
        $participant = Participant::getFromHash($_COOKIE[$cookie_name]);
        if($participant != null) {
            progressToPage($participant);
            return;
        }
    }

    require_once __DIR__ . '/front-end/html/login.html';

}, 'get');

Route::add('/login', function () use ($cookie_name) {
    /*
     * 1. Obtain uid
     * 2. Check validity
     * 3. Redirect to proper page (check progress)
     */
    if(isset($_POST['uid']) && isset($_POST['password'])) {
        $participant = Participant::getFromHash($_POST['uid']);
        if($participant != null && Participant::authenticate($_POST['uid'], $_POST['password'])) {
            setcookie($cookie_name, $participant->getUIDHash(), secure: true);
            progressToPage($participant);
        } else {
            $title = "Error";
            $head = "Error";
            $message = "You entered an invalid id";
                require_once 'front-end/templates/error.php';
        }
    } else {
        $title = "Error";
        $head = "Error";
        $message = "Not all the data sign-in data reached our server. Please try again to login.";
        require_once 'front-end/templates/error.php';
    }
}, 'post');

Route::add('/logout', function () use ($cookie_name) {
    unset($_COOKIE[$cookie_name]);
    setcookie($cookie_name, null, path: '/');
    header("Location: /");
});

Route::add('/participant', function () use ($cookie_name) {
    if(isset($_COOKIE[$cookie_name])) {
        $hash = $_COOKIE[$cookie_name];
        $participant = Participant::getFromHash($hash);
        if($participant != null && $participant->getProgress() == Participant::$PROGRESS_OPTIONS["FINISHED"]) {
            require_once __DIR__ . '/front-end/templates/participant_profile.php';
            return;
        }
    }
    StatusCodes::HTTPStatus(401);
    $title = "401 - Unauthorized";
    $head = "401";
    $message = "You do not have access to this page";
    require_once 'front-end/templates/error.php';
});

Route::add('/participant/collected_data', function () use ($cookie_name) {
    if(isset($_COOKIE[$cookie_name])) {
        $hash = $_COOKIE[$cookie_name];
        $participant = Participant::getFromHash($hash);
        if($participant != null && $participant->getProgress() == Participant::$PROGRESS_OPTIONS["FINISHED"]) {
            require_once 'front-end/templates/collected_data.php';
            return;
        }
    }
    StatusCodes::HTTPStatus(401);
    $title = "401 - Unauthorized";
    $head = "401";
    $message = "You do not have access to this page";
    require_once 'front-end/templates/error.php';
});

Route::add('/participant/revoke', function () use ($cookie_name) {
    /*
     * 1. Get the participant.
     * 2. Remove participant.
     * 3. Send a success message back.
     */
    if(isset($_COOKIE[$cookie_name])) {
        $hash = $_COOKIE[$cookie_name];
        $participant = Participant::getFromHash($hash);
        if($participant != null) {
            $participant->remove();
            unset($_COOKIE[$cookie_name]);
            setcookie($cookie_name, null, path: '/');
            // TODO: Show a success message and redirect to the home page of the site.
            $message = "All your data has been removed successfully.";
            header('Refresh: 3; URL=/');
            require_once 'front-end/templates/success.php';
            return;
        }
    }
    StatusCodes::HTTPStatus(401);
    $title = "401 - Unauthorized";
    $head = "401";
    $message = "You do not have access to this page";
    require_once 'front-end/templates/error.php';
});

// ANALYSIS

/**
 * The home page, this will probably contain the introduction and the consent form.
 */
Route::add('/', function () use ($cookie_name) {
    // Check if the participant already has a cookie set, if so redirect them to their respective page.
    if(isset($_COOKIE[$cookie_name])) {
        $participant = Participant::getFromHash($_COOKIE[$cookie_name]);
        if ($participant != null) {
            progressToPage($participant);
            return;
        }
    }
    require_once 'front-end/html/Home.html';
});

Route::add('/credentials', function () use ($cookie_name) {
    /*
     * 1. Generate UID or check if the participant already exists...
     * 2. Generate a cookie that links session to this UID.
     * 3. Display the UID and password to the user, so they can note this down.
     */
    $exists = false;
    $participant = null;
    if(isset($_COOKIE[$cookie_name]))  {
        $participant = Participant::getFromHash($_COOKIE[$cookie_name]);
        if ($participant != null) {
            $exists = true;
        }
    }
    if(!$exists) {
        $participant = new Participant();
        $participant->save();
        $hash = $participant->getUIDHash();
        $password = $participant->getPlaintextPassword();
        $participant->setPlaintextPassword(null);
        setcookie($cookie_name, $hash, secure: true);
        require_once 'front-end/templates/credentials.php';
    } else {
        progressToPage($participant);
    }
});

Route::add('/pre-analysis', function () use ($cookie_name) {
    /*
     * 1. Generate UID or check if the participant already exists....
     * 2. Generate Cookie that links session to this UID.
     * 3. Retrieve pre-analysis questions.
     * 4. Render HTML document
     * 5. Send page to browser.
     */
    $exists = false;
    $participant = null;
    if(isset($_COOKIE[$cookie_name]))  {
        $participant = Participant::getFromHash($_COOKIE[$cookie_name]);
        if ($participant != null) {
            $exists = true;
        }
    }
    if (!$exists) {
        StatusCodes::HTTPStatus(401);
        $title = "401 - Unauthorized";
        $head = "401";
        $message = "You do not have access to this page";
        require_once 'front-end/templates/error.php';
    } else if ($participant->getProgress() != Participant::$PROGRESS_OPTIONS["PRE_ANALYSIS"]) {
            progressToPage($participant);
    } else {
        include 'front-end/templates/pre-analysis.php';
    }

});

Route::add('/pre-analysis/submit', function () use ($cookie_name) {
    /*
     * 1. Obtain UID from cookie.
     * 2. Retrieve answers of the questions.
     * 3. Store answers to questions in the database.
     * 4. Redirect to the analysis page.
     */
    if(isset($_COOKIE[$cookie_name])) {
        $participant = Participant::getFromHash($_COOKIE[$cookie_name]);
        if ($participant == null) {
            $title = "Error";
            $head = "Error";
            $message = "You did not start the survey correctly, please go to: https://tommeulenkamp.nl. Otherwise, make sure your cookies are enabled";
            require_once 'front-end/templates/error.php';
        } else if (Participant::$PROGRESS_OPTIONS[$participant->getProgress()] == "PRE_ANALYSIS") {
            $participant->setProgress("ANALYSIS");
            $participant->save();
            $qids = [];
            $answers = [];
            foreach($_POST as $key => $value) {
                if(is_int($key)) {
                    array_push($qids, $key);
                    $int_val = intval($value);
                    if($int_val != 0) {
                        array_push($answers, $int_val);
                    } else {
                        $title = "Error";
                        $head = "Error";
                        $message = "Incorrect value types have been send to our server.";
                        require_once 'front-end/templates/error.php';
                    }
                }
            }
            $success = $participant->saveAnswers($qids, $answers);
            if($success) {
                header("Location: /analysis");
            } else {
                $title = "Error";
                $head = "Error";
                $message = "We failed to save your answer please try again.";
                require_once 'front-end/templates/error.php';
            }

        } else {
            // Participant isn't on the correct page.
            // They shouldn't be submitting since they've already completed the pre analysis survey.
            progressToPage($participant);
        }
    } else {
        StatusCodes::HTTPStatus(401);
        $title = "401 - Unauthorized";
        $head = "401";
        $message = "If you are new here, please go to: https://tommeulenkamp.nl. Otherwise, make sure your cookies are enabled";
        require_once 'front-end/templates/error.php';
    }

}, 'post');

/**
 * The analysis page where users can upload their downloaded Google data and have a look at the generated statistics.
 */
Route::add('/analysis', function () use ($cookie_name) {
    if(!isset($_COOKIE[$cookie_name])) {
        StatusCodes::HTTPStatus(401);
        $title = "401 - Unauthorized";
        $head = "401";
        $message = "If you are new here, please go to: https://tommeulenkamp.nl. Otherwise, make sure your cookies are enabled";
        require_once 'front-end/templates/error.php';
        return;
    }

    $participant = Participant::getFromHash($_COOKIE[$cookie_name]);
    if ($participant == null) {
        StatusCodes::HTTPStatus(401);
        $title = "401 - Unauthorized";
        $head = "401";
        $message = "It seems that the survey data is corrupted, please try to start again.";
        require_once 'front-end/templates/error.php';
    } elseif ($participant->getProgress() != Participant::$PROGRESS_OPTIONS["ANALYSIS"]) {
        progressToPage($participant);
    } else {
        StatusCodes::HTTPStatus(200);
        require_once __DIR__ . '/front-end/templates/analyzer.php';
    }

});

/**
 * Loads the PDF that contains the instructions on how to generate the Takeout PDF.
 */
Route::add('/analysis/instructions', function () {
    require_once __DIR__ . '/front-end/html/instructions.html';
});

/**
 * Contains the analysis and survey. This page will probably do most of the heavy lifting, since everything
 * should happen client-side.
 */
Route::add('/post-analysis', function () use ($cookie_name) {
    /*
     * 1. Check if the cookie with the UID is still present and valid.
     * 2. Retrieve the available post-analysis questions.
     * 3. Render HTML document.
     * 4. Send page to browser
     */

    if(!isset($_COOKIE[$cookie_name])) {
        StatusCodes::HTTPStatus(401);
        $title = "401 - Unauthorized";
        $head = "401";
        $message = "If you are new here, please go to: https://tommeulenkamp.nl. Otherwise, make sure your cookies are enabled";
        require_once 'front-end/templates/error.php';
        return;
    }

    $participant = Participant::getFromHash($_COOKIE[$cookie_name]);
    if ($participant == null) {
        StatusCodes::HTTPStatus(401);
        $title = "401 - Unauthorized";
        $head = "401";
        $message = "It seems that the survey data is corrupted, please try to start again.";
        require_once 'front-end/templates/error.php';
    }
    if ($participant->getProgress() == Participant::$PROGRESS_OPTIONS["ANALYSIS"]) {
        $participant->setProgress("POST_ANALYSIS");
        $participant->save();
    }
    if ($participant->getProgress() != Participant::$PROGRESS_OPTIONS["POST_ANALYSIS"]) {
        progressToPage($participant);
    } else {
        StatusCodes::HTTPStatus(200);
        include_once 'front-end/templates/post-analysis.php';
    }
});

Route::add('/post-analysis/submit', function () use ($cookie_name) {
    /*
     * 1. Obtain UID from cookie.
     * 2. Retrieve answers of the questions.
     * 3. Store the answers in the database.
     */
    if(!isset($_COOKIE[$cookie_name])) {
        StatusCodes::HTTPStatus(401);
        $title = "401 - Unauthorized";
        $head = "401";
        $message = "If you are new here, please go to: https://tommeulenkamp.nl. Otherwise, make sure your cookies are enabled";
        require_once 'front-end/templates/error.php';
    } elseif (Participant::getFromHash($_COOKIE[$cookie_name]) == null) {
        StatusCodes::HTTPStatus(401);
        $title = "401 - Unauthorized";
        $head = "401";
        $message = "It seems that the survey data is corrupted, please try to start again.";
        require_once 'front-end/templates/error.php';
    } elseif (Participant::getFromHash($_COOKIE[$cookie_name])->getProgress() != Participant::$PROGRESS_OPTIONS["POST_ANALYSIS"]) {
        progressToPage(Participant::getFromHash($_COOKIE[$cookie_name]));
    } else {
        $participant = Participant::getFromHash($_COOKIE[$cookie_name]);
        StatusCodes::HTTPStatus(200);
        $participant->setProgress("FINISHED");
        $participant->save();
        $qids = [];
        $answers = [];
        foreach($_POST as $key => $value) {
            if(is_int($key)) {
                array_push($qids, $key);
                $int_val = intval($value);
                if($int_val != 0) {
                    array_push($answers, $int_val);
                } else {
                    $value = filter_var($_POST[$key], FILTER_SANITIZE_STRING);
                    if (($value == null) || is_bool($value)) {
                        $title = "Error";
                        $head = "Not Allowed Input";
                        $message = "You are not allowed to enter certain characters in {$key} => {$value} ";
                        require_once 'front-end/templates/error.php';
                    } else {
                        array_push($answers, $value);
                    }
                }
            }
        }
        $success = $participant->saveAnswers($qids, $answers);
        if($success) {
            header("Location: /finished");
        } else {
            $title = "Error";
            $head = "Error";
            $message = "We failed to save your answer please try again.";
            require_once 'front-end/templates/error.php';
        }
    }
}, 'post');

Route::add('/finished', function () {
    header('Refresh: 3; URL=/participant');
    $message = "You've finished the survey!<br>You can now proceed to your personal page, where you can request a PDF of your input and revoke access if needed.";
    require_once 'front-end/templates/success.php';
});

Route::add('/rerun_analysis', function () use ($cookie_name) {
    if(!isset($_COOKIE[$cookie_name])) {
        StatusCodes::HTTPStatus(401);
        $title = "401 - Unauthorized";
        $head = "401";
        $message = "If you are new here, please go to: https://tommeulenkamp.nl. Otherwise, make sure your cookies are enabled";
        require_once 'front-end/templates/error.php';
        return;
    }

    $participant = Participant::getFromHash($_COOKIE[$cookie_name]);
    if ($participant == null) {
        StatusCodes::HTTPStatus(401);
        $title = "401 - Unauthorized";
        $head = "401";
        $message = "It seems that the survey data is corrupted, please try to start again.";
        require_once 'front-end/templates/error.php';
    } elseif ($participant->getProgress() != Participant::$PROGRESS_OPTIONS["FINISHED"]) {
        progressToPage($participant);
    } else {
        StatusCodes::HTTPStatus(200);
        require_once __DIR__ . '/front-end/templates/analyzer.php';
    }
});

// METHODS

Route::pathNotFound(function ($path) {
    StatusCodes::HTTPStatus(404);
    $title = "404 - Not Found";
    $head = "404";
    $message = "The path $path couldn't be found.";
    require_once 'front-end/templates/error.php';
});

Route::methodNotAllowed(function ($path, $method) {
    StatusCodes::HTTPStatus(405);
    $title = "405 - Method Not Allowed";
    $head = "405";
    $message = "The path $path exists, but does not allow the method $method.";
    require_once 'front-end/templates/error.php';
});

// RUN

Route::run();
