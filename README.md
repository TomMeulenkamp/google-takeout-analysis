# CSCC Project

This is the public repository for the "Cybersecurity and Cybercrime" final project of project group 1. This project aims at giving the participants in our research better insights into the data that Google has collected on them.

This repository combines two functionalities. It is a questionnaire system, which hosts questions that can be answered by participants. Secondly, it contains an analysis tool that can read the produced zip-file by Google Takout (the service that dumps all the data that Google has about you.) this tool will visualize certain data points that Google has collected. The visualisation mainly consist out of charts.

We believe that it is important to open-source our code, such that people can see that we do not collect any of their personal Google data on them. Secondly, we want to make sure that other people can work and built on the tool that we have created.